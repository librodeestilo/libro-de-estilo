dataSource {
	pooled = true
	driverClassName = "com.mysql.jdbc.Driver"
	username = "libro"
	password = "documental"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
}
hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = false
	cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
	//    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "update" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost/libro?useUnicode=yes&characterEncoding=UTF-8"
		}
	}
	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost/libro?useUnicode=yes&characterEncoding=UTF-8"
		}
	}
	production {
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost/libro?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true"
			properties {
				validationQuery="select 1"
				testWhileIdle=true
				testOnBorrow = true
				testOnReturn = false
				timeBetweenEvictionRunsMillis=60000
			}
		}
	}
}
