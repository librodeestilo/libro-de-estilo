class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        
	"/robots.txt" (view: "robots")
        "/recursos"(controller:"libro", action:"recursos")
        "/acercade"(controller:"info", action:"acerca_de")
        "/acceder"(controller:"login", action:"auth")
        "/registro"(controller:"register", action:"index")
        "/libro/ficha"(controller:"medio", action:"edit_1")
        "/libro/intro"(controller:"libro", action:"edit_2")
        "/libro/estilo"(controller:"libro", action:"edit_3")
        "/libro/etica"(controller:"libro", action:"edit_4")
        "/info/$action"(controller:"info")
        
        "/pdf/$slug" (controller:"libro", action:"view_pdf")
        "/html/$slug" (controller:"libro", action:"view_html")
        "/base/$usuario" (controller:"libro", action: "base")
        "/logo/$slug"(controller:"libro", action:"logo")
        "/$slug(.$format)"(controller:"libro", action:"view_html")
        
	//"/libro/$slug(.pdf)"(controller:"libro", action:"pdf")
	//"/libro/$slug(.html)"(controller:"libro", action:"html")
		
        //name pdf: "/libro/$user.pdf" {
        //	controller:"libro"
        //	action:"pdf"
        //}
				
        //"/"(view:"/index")
        "/"(controller:"info", action:"inicio")
        "500"(view:'/error')
    }
}
