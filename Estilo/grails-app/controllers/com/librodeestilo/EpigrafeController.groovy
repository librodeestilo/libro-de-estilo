package com.librodeestilo

import org.springframework.dao.DataIntegrityViolationException

class EpigrafeController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [epigrafeInstanceList: Epigrafe.list(params), epigrafeInstanceTotal: Epigrafe.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[epigrafeInstance: new Epigrafe(params)]
			break
		case 'POST':
	        def epigrafeInstance = new Epigrafe(params)
	        if (!epigrafeInstance.save(flush: true)) {
	            render view: 'create', model: [epigrafeInstance: epigrafeInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), epigrafeInstance.id])
	        redirect action: 'show', id: epigrafeInstance.id
			break
		}
    }

    def show() {
        def epigrafeInstance = Epigrafe.get(params.id)
        if (!epigrafeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
            redirect action: 'list'
            return
        }

        [epigrafeInstance: epigrafeInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def epigrafeInstance = Epigrafe.get(params.id)
	        if (!epigrafeInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [epigrafeInstance: epigrafeInstance]
			break
		case 'POST':
	        def epigrafeInstance = Epigrafe.get(params.id)
	        if (!epigrafeInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (epigrafeInstance.version > version) {
	                epigrafeInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'epigrafe.label', default: 'Epigrafe')] as Object[],
	                          "Another user has updated this Epigrafe while you were editing")
	                render view: 'edit', model: [epigrafeInstance: epigrafeInstance]
	                return
	            }
	        }

	        epigrafeInstance.properties = params

	        if (!epigrafeInstance.save(flush: true)) {
	            render view: 'edit', model: [epigrafeInstance: epigrafeInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), epigrafeInstance.id])
	        redirect action: 'show', id: epigrafeInstance.id
			break
		}
    }

    def delete() {
        def epigrafeInstance = Epigrafe.get(params.id)
        if (!epigrafeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
            redirect action: 'list'
            return
        }

        try {
            epigrafeInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'epigrafe.label', default: 'Epigrafe'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
