package com.librodeestilo

import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured


class InfoController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    @Secured(["hasRole('ROLE_ADMIN')"])
    def index() {
        redirect action: 'list', params: params
    }
	
    @Secured(["hasRole('ROLE_ADMIN')"])
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [infoInstanceList: Info.list(params), infoInstanceTotal: Info.count()]
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def create() {
        switch (request.method) {
        case 'GET':
            [infoInstance: new Info(params)]
            break
        case 'POST':
            def infoInstance = new Info(params)
            if (!infoInstance.save(flush: true)) {
                render view: 'create', model: [infoInstance: infoInstance]
                return
            }

            flash.message = message(code: 'default.created.message', args: [message(code: 'info.label', default: 'Info'), infoInstance.id])
            redirect action: 'show', id: infoInstance.id
            break
        }
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def show() {
        def infoInstance = Info.get(params.id)
        if (!infoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'info.label', default: 'Info'), params.id])
            redirect action: 'list'
            return
        }

        [infoInstance: infoInstance]
    }

    def v(Integer id) {
        def infoInstance = Info.get(id)
        if (!infoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'info.label', default: 'Info'), params.id])
            redirect action: 'list'
            return
        }

        [infoInstance: infoInstance]
    }
    
    def acerca_de() {
    }
    
    def inicio() {
    }
	
    @Secured(["hasRole('ROLE_ADMIN')"])
    def edit() {
        switch (request.method) {
        case 'GET':
            def infoInstance = Info.get(params.id)
            if (!infoInstance) {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'info.label', default: 'Info'), params.id])
                redirect action: 'list'
                return
            }

            [infoInstance: infoInstance]
            break
        case 'POST':
            def infoInstance = Info.get(params.id)
            if (!infoInstance) {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'info.label', default: 'Info'), params.id])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (infoInstance.version > version) {
                    infoInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [message(code: 'info.label', default: 'Info')] as Object[],
	                          "Another user has updated this Info while you were editing")
                    render view: 'edit', model: [infoInstance: infoInstance]
                    return
                }
            }

            infoInstance.properties = params

            if (!infoInstance.save(flush: true)) {
                render view: 'edit', model: [infoInstance: infoInstance]
                return
            }

            flash.message = message(code: 'default.updated.message', args: [message(code: 'info.label', default: 'Info'), infoInstance.id])
            redirect action: 'show', id: infoInstance.id
            break
        }
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def delete() {
        def infoInstance = Info.get(params.id)
        if (!infoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'info.label', default: 'Info'), params.id])
            redirect action: 'list'
            return
        }

        try {
            infoInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'info.label', default: 'Info'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'info.label', default: 'Info'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
