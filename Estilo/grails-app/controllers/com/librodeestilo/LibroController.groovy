package com.librodeestilo

import grails.plugin.springsecurity.annotation.Secured
import com.librodeestilo.auth.User
import org.springframework.dao.DataIntegrityViolationException

class LibroController {
    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def springSecurityService
    def wkhtmltoxService

    def index() {
        redirect action: 'html', params: params
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [libroInstanceList: Libro.list(params), libroInstanceTotal: Libro.count()]
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def create() {
        def master= Master.first()
        log.debug(master)
        log.debug(params)

        switch (request.method) {
        case 'GET':
            [master: master, libroInstance: new Libro(params)]
            break
        case 'POST':

            def libroInstance = new Libro(params)
            def props= params.propuesta.keySet().findAll{it.isInteger()}
            libroInstance.propuestas= Propuesta.getAll(props)

            if (!libroInstance.save(flush: true)) {
                render view: 'create', model: [master:master, libroInstance: libroInstance]
                return
            }

            flash.message = message(code: 'default.created.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    libroInstance.id
                ])
            redirect action: 'show', id: libroInstance.id
            break
        }
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def show() {
        def master= Master.first()
        def libroInstance = Libro.get(params.id)
        if (!libroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    params.id
                ])
            redirect action: 'list'
            return
        }

        [master: master, libroInstance: libroInstance]
    }
    
    def base() {
        //cache shared: true, validUntil: new Date()+1
        def master= Master.first()
        def user= params.usuario? User.findByUsername(params.usuario):null
        user= user?: springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        log.debug("H User: "+user?.username)
        if (!user){
            redirect controller:'info', action:'inicio'
        }

        log.debug("H Slug: "+params.slug)
        
        def medio= user ? Medio.findByResponsable(user) : params.slug ? Medio.findBySlug(params.slug) : null
        //		def libroInstance = Libro.get(params.id)
        log.debug("H Medio: "+medio?.id)
        if (!medio){
            redirect controller:'medio', action:'edit_1'
        }

        log.debug("H Libro: "+medio?.libro?.id)        
        
        def libroInstance= medio ? medio.libro : null
        
        [master: master, medio: medio, libroInstance: libroInstance]
    }
    
    def html() {
        //cache shared: true, validUntil: new Date()+1
        def master= Master.first()
        def medio= Medio.findBySlug(params.slug)
        //		def libroInstance = Libro.get(params.id)
        def libroInstance= medio ? medio.libro: master
		
        [master: master, medio: medio, libroInstance: libroInstance]
    }
   
    def logo() {
        def medio= Medio.findBySlug(params.slug)
        log.debug("Enviando logo: "+params.slug)
        response.setIntHeader('Content-length', medio.logo.length)
        response.contentType = 'image/png' // or the appropriate image content type
        if (medio && medio.logo){
            response.outputStream.write(medio.logo)
            response.outputStream.flush()
        }
    }

    def recursos() {
        //cache shared: true, validUntil: new Date()+1
        def master= Master.first()
		
        [master: master, libroInstance: master]
    }
    
    def master() {
        //cache shared: true, validUntil: new Date()+1
        def master= Master.first()

        [master: master, libroInstance: null]
    }

    def footer_view(){
    }
    
    def view_html(){
        log.debug("html_view: "+params.slug)
        if (!params.slug){
            redirect controller: 'info', action: 'inicio'
        }
        log.debug(params.format)
        def medio= Medio.findBySlug(params.slug)
        def master= Master.first()
        def libroInstance= medio?.libro
        log.debug("slug:"+medio?.slug)
 
        if (params.format=="pdf"){
            view_pdf(params)
        }
        else{
            [master: master, medio: medio, libroInstance: libroInstance]
        }
    }
        
    def view_pdf(params){
        log.debug("pdf_view: "+params.slug)
        log.debug(params.format)
        def medio= Medio.findBySlug(params.slug)
        def master= Master.first()
        def libroInstance= medio?.libro

        log.debug("Libro: "+libroInstance?.id)

        //[master: master, medio: medio, libroInstance: libroInstance]
        //ByteArrayOutputStream bytes = pdfRenderingService.render(template: "/libro/pdf", model: [master: master, libroInstance: libroInstance])
        //renderPdf([template: '/libro/pdf', model:[master: master, medio: medio, libroInstance: libroInstance], filename: "test.pdf"])

        def byte[] pdfData
        def nombre="libropdf"+(libroInstance?libroInstance.id:"0")
        log.debug(nombre)
        def cache= new File("cache").mkdirs()
        File pdf= new File("cache", nombre+".pdf")
        def checktime= new Date().minus(1).time
        
        //log.debug(libroInstance.lastUpdated?.getTime())
        //log.debug(pdf.lastModified())
        if (!pdf.exists() || pdf.lastModified()<=checktime || libroInstance.lastUpdated.getTime()>pdf.lastModified()){
            log.debug("Generando en view-pdf: "+nombre)
            def base= grailsApplication.config.grails.serverURL
            
            def cmd="/usr/local/bin/wkhtmltopdf "
            cmd= cmd+"-T 20 -L 20 -R 20 "
            
            cmd= cmd+"--footer-html "+base+"/libro/footer_view "
            //cmd= cmd+"--header-html "+base+"/libro/header_view/ "
            cmd= cmd+base+"/libro/view_html/?format=html&slug="+medio.slug+" "+pdf.getAbsolutePath()
            log.debug(cmd)
            def process= cmd.execute()
            process.waitForProcessOutput()
        }
        else{
            log.debug("Enviando desde caché")
        }

        if (pdf.isFile()){
            pdfData= pdf.bytes
                
            response.setContentType("application/pdf")
            response.setHeader("Content-disposition", "filename=\"librodeestilo.pdf\"")
            response.setContentLength(pdfData.size())

            def outputStream = null
            try {
                outputStream = response.outputStream
                outputStream << pdfData
                outputStream.flush()

            } catch (IOException e){
                log.debug('getImage() - Canceled download?', e)
            } finally {
                if (outputStream != null){
                    try {
                        outputStream.close()
                    } catch (IOException e) {
                        log.debug('Exception on close', e)
                    }
                }
            }
        }
    }
    
    def pdf() {
        def master= Master.first()
        def user= springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        log.debug("User: "+user?.username)
        
        log.debug("Slug: "+params.slug)
        
        def medio= user ? Medio.findByResponsable(user) : params.slug ? Medio.findBySlug(params.slug) : null
        //		def libroInstance = Libro.get(params.id)
        log.debug("Medio: "+medio?.id)
        
        def libroInstance= medio ? medio.libro : null
        
        log.debug("Libro: "+libroInstance?.id)

        //[master: master, medio: medio, libroInstance: libroInstance]
        //ByteArrayOutputStream bytes = pdfRenderingService.render(template: "/libro/pdf", model: [master: master, libroInstance: libroInstance])
        //renderPdf([template: '/libro/pdf', model:[master: master, medio: medio, libroInstance: libroInstance], filename: "test.pdf"])

        def byte[] pdfData
        def nombre="libropdf"+(libroInstance?libroInstance.id:"0")
        def cache= new File("cache").mkdirs()
        File pdf= new File("cache", nombre+".pdf")
        def checktime= new Date().minus(1).time
        
        log.debug(libroInstance.lastUpdated.getTime())
        log.debug(pdf.lastModified())
        if (true || !pdf.exists() || pdf.lastModified()<=checktime || libroInstance.lastUpdated.getTime()>pdf.lastModified()){
            log.debug("Generando "+nombre)
            pdfData = wkhtmltoxService.makePdf(
                view:"/libro/pdf",
                model:[master: master, medio: medio, libroInstance: libroInstance],
                //header:"/pdf/someHeader",
                footer:"/libro/pdffooter",
                marginLeft:20,
                marginTop:35,
                marginBottom:20,
                marginRight:20,
                headerSpacing:10
            )
            pdf.withOutputStream{
                stream -> stream <<pdfData
            }
        }
        else{
            log.debug("Enviando caché:"+pdf.getAbsolutePath())
            pdfData= pdf.bytes
        }
                
        response.setContentType("application/pdf")
        response.setHeader("Content-disposition", "filename=\"librodeestilo.pdf\"")
        response.setContentLength(pdfData.size())

        def outputStream = null
        try {
            outputStream = response.outputStream
            outputStream << pdfData
            outputStream.flush()

        } catch (IOException e){
            log.debug('getImage() - Canceled download?', e)
        } finally {
            if (outputStream != null){
                try {
                    outputStream.close()
                } catch (IOException e) {
                    log.debug('Exception on close', e)
                }
            }
        }
        return
        
    }

    @Secured(["hasRole('ROLE_USER')"])
    def edit() {
        def master= Master.first()
        log.debug(params)

        def user = springSecurityService.currentUser
        log.debug(user)
        
        def medio= Medio.findByResponsable(user)
        if (!medio) {
            medio= new Medio()
            medio.responsable= user
            medio.nombre= user.username
            medio.slug= user.username
            medio.save(flush:true)
        }
		
        log.debug(medio.id)
		
        if (!medio.libro) {
            log.debug("Creando libro para "+user)
            def libro= new Libro()
            libro.medio= medio
            libro.propuestas= Propuesta.getAll()
            if (libro.save(flush:true)) {
                log.debug("Grabado ${libro.id}")
            }
            medio.libro= libro
            medio.save(flush:true)
        }

        def libroInstance= medio.libro
		
        log.debug(libroInstance.id)
				
        switch (request.method) {
        case 'GET':
            [master:master, libroInstance: libroInstance]
            break
        case 'POST':
            //				def libroInstance = Libro.get(params.id)
            if (!libroInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'libro.label', default: 'Libro'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (libroInstance.version > version) {
                    libroInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'libro.label', default: 'Libro')] as Object[],
								"Another user has updated this Libro while you were editing")
                    render view: 'edit', model: [master: master, libroInstance: libroInstance]
                    return
                }
            }

            libroInstance.properties = params
            def props= params.propuesta.keySet().findAll{it.isInteger()}

            libroInstance.propuestas= Propuesta.getAll(props)

            if (!libroInstance.save(flush: true)) {
                render view: 'edit', model: [master: master, libroInstance: libroInstance]
                return
            }

            flash.message = message(code: 'default.updated.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    libroInstance.id
                ])
            redirect action: 'edit', id: libroInstance.id
            break
        }
    }
 
    @Secured(["hasRole('ROLE_USER')"])
    def edit_2() {
        def master= Master.first()
        log.debug(params)

        def user = springSecurityService.currentUser
        log.debug(user)
        
        def medioInstance= Medio.findByResponsable(user)
        def libroInstance= medioInstance.libro
		
        log.debug(libroInstance?.id)
				
        switch (request.method) {
        case 'GET':
            [master:master, libroInstance: libroInstance]
            break
        case 'POST':
            //				def libroInstance = Libro.get(params.id)
            if (!libroInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'libro.label', default: 'Libro'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (libroInstance.version > version) {
                    libroInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'libro.label', default: 'Libro')] as Object[],
								"Another user has updated this Libro while you were editing")
                    render view: 'edit_2', model: [master: master, libroInstance: libroInstance]
                    return
                }
            }

            libroInstance.properties = params

            log.debug(libroInstance.descripcion)
            
            if (!libroInstance.save(flush: true)) {
                render view: 'edit_2', model: [master: master, libroInstance: libroInstance]
                return
            }

            /*
            flash.message = message(code: 'default.updated.message', args: [
            message(code: 'libro.label', default: 'Libro'),
            libroInstance.id
            ])
             */
            flash.message= "El Libro de estilo de ${medioInstance.nombre} se ha actualizado"
            redirect action: 'edit_3', id: libroInstance.id
            break
        }
    }
   
    @Secured(["hasRole('ROLE_USER')"])
    def edit_3() {
        def master= Master.first()
        log.debug(params)

        def user = springSecurityService.currentUser
        log.debug(user)
                
        def medioInstance= Medio.findByResponsable(user)
        def libroInstance= medioInstance.libro
		
        log.debug(libroInstance.id)
				
        switch (request.method) {
        case 'GET':
            [master:master, libroInstance: libroInstance]
            break
        case 'POST':
            //				def libroInstance = Libro.get(params.id)
            if (!libroInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'libro.label', default: 'Libro'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (libroInstance.version > version) {
                    libroInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'libro.label', default: 'Libro')] as Object[],
								"Another user has updated this Libro while you were editing")
                    render view: 'edit_3', model: [master: master, libroInstance: libroInstance]
                    return
                }
            }

            libroInstance.properties = params
            def props= params.propuesta.keySet().findAll{it.isInteger()}
            libroInstance.estilo= Propuesta.getAll(props)

  
            if (!libroInstance.save(flush: true)) {
                render view: 'edit_3', model: [master: master, libroInstance: libroInstance]
                return
            }

            /*
            flash.message = message(code: 'default.updated.message', args: [
            message(code: 'libro.label', default: 'Libro'),
            libroInstance.id
            ])
             */
            flash.message= "El Libro de estilo de ${medioInstance.nombre} se ha actualizado"
            redirect action: 'edit_4', id: libroInstance.id
            break
        }
    }
    
    @Secured(["hasRole('ROLE_USER')"])
    def edit_4() {
        def master= Master.first()
        log.debug(params)

        def user = springSecurityService.currentUser
        log.debug(user)
                
        def medioInstance= Medio.findByResponsable(user)
        def libroInstance= medioInstance.libro
		
        log.debug(libroInstance.id)
				
        switch (request.method) {
        case 'GET':
            [master:master, libroInstance: libroInstance]
            break
        case 'POST':
            //				def libroInstance = Libro.get(params.id)
            if (!libroInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'libro.label', default: 'Libro'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (libroInstance.version > version) {
                    libroInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'libro.label', default: 'Libro')] as Object[],
								"Another user has updated this Libro while you were editing")
                    render view: 'edit_4', model: [master: master, libroInstance: libroInstance]
                    return
                }
            }

            libroInstance.properties = params
            def props= params.propuesta.keySet().findAll{it.isInteger()}
            libroInstance.etica= Propuesta.getAll(props)

  
            if (!libroInstance.save(flush: true)) {
                render view: 'edit_4', model: [master: master, libroInstance: libroInstance]
                return
            }

            /*
            flash.message = message(code: 'default.updated.message', args: [
            message(code: 'libro.label', default: 'Libro'),
            libroInstance.id
            ])
             */
            flash.message= "El Libro de estilo de ${medioInstance.nombre} se ha actualizado"
            redirect action: 'base', id: libroInstance.id
            break
        }
    }
    
    @Secured(["hasRole('ROLE_ADMIN')"])
    def delete() {
        def libroInstance = Libro.get(params.id)
        if (!libroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    params.id
                ])
            redirect action: 'list'
            return
        }

        try {
            libroInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    params.id
                ])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [
                    message(code: 'libro.label', default: 'Libro'),
                    params.id
                ])
            redirect action: 'show', id: params.id
        }
    }
}
