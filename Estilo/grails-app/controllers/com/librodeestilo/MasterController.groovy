package com.librodeestilo

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.dao.DataIntegrityViolationException


class MasterController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [masterInstanceList: Master.list(params), masterInstanceTotal: Master.count()]
    }

	@Secured(["hasRole('ROLE_ADMIN')"])
    def create() {
		switch (request.method) {
		case 'GET':
        	[masterInstance: new Master(params)]
			break
		case 'POST':
	        def masterInstance = new Master(params)
	        if (!masterInstance.save(flush: true)) {
	            render view: 'create', model: [masterInstance: masterInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'master.label', default: 'Master'), masterInstance.id])
	        redirect action: 'show', id: masterInstance.id
			break
		}
    }

    def show() {
        def masterInstance = Master.get(params.id)
        if (!masterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'master.label', default: 'Master'), params.id])
            redirect action: 'list'
            return
        }

        [masterInstance: masterInstance]
    }
	
	def v() {
		def masterInstance = Master.first()
		if (!masterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'master.label', default: 'Master'), params.id])
			redirect action: 'list'
			return
		}

		[master: masterInstance]
	}

	@Secured(["hasRole('ROLE_ADMIN')"])
    def edit() {
		switch (request.method) {
		case 'GET':
	        def masterInstance = Master.get(params.id)
	        if (!masterInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'master.label', default: 'Master'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [masterInstance: masterInstance]
			break
		case 'POST':
	        def masterInstance = Master.get(params.id)
	        if (!masterInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'master.label', default: 'Master'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (masterInstance.version > version) {
	                masterInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'master.label', default: 'Master')] as Object[],
	                          "Another user has updated this Master while you were editing")
	                render view: 'edit', model: [masterInstance: masterInstance]
	                return
	            }
	        }

	        masterInstance.properties = params

	        if (!masterInstance.save(flush: true)) {
	            render view: 'edit', model: [masterInstance: masterInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'master.label', default: 'Master'), masterInstance.id])
	        redirect action: 'show', id: masterInstance.id
			break
		}
    }

	@Secured(["hasRole('ROLE_ADMIN')"])
    def delete() {
        def masterInstance = Master.get(params.id)
        if (!masterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'master.label', default: 'Master'), params.id])
            redirect action: 'list'
            return
        }

        try {
            masterInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'master.label', default: 'Master'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'master.label', default: 'Master'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
