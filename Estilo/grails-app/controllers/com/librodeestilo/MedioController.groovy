package com.librodeestilo

import com.librodeestilo.auth.User
import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured

class MedioController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def springSecurityService

    def index() {
        redirect action: 'show', params: params
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [medioInstanceList: Medio.list(params), medioInstanceTotal: Medio.count()]
    }

    @Secured(["hasRole('ROLE_USER')"])
    def createMedia() {
        def	user = springSecurityService.currentUser
        def libroInstance= null
        log.debug(user)

        def medio= Medio.findByResponsable(user)
        if (!medio) {
            def medioInstance = new Medio()
            medioInstance.responsable= user

            if (!medioInstance.save(flush: true)) {
                log.debug("No grabado para ${user.id}")
                flash.message = message(code: 'default.error.message', args: [
                        message(code: 'medio.label', default: 'Medio')
                    ])
                redirect controller: 'libro', action: 'master'
            }

            libroInstance = new Libro()
            libroInstance.medio= medioInstance

            if (!libroInstance.save(flush: true)) {
                log.error("No se ha creado libro para ${user.id}")
                flash.message = message(code: 'default.error.message', args: [
                        message(code: 'libro.label', default: 'Libro')
                    ])
                redirect controller: 'libro', action: 'master'
            }
            libroInstance.propuestas= Propuesta.getAll()
			
            medioInstance.libro= libroInstance
            medioInstance.save(flush:true)
        }
        else {
            libroInstance= medio.libro 
        }
        redirect controller: 'libro', action: 'edit', id: libroInstance.id		
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def create() {
        switch (request.method) {
        case 'GET':
            [medioInstance: new Medio(params)]
            break
        case 'POST':
            def medioInstance = new Medio(params)
            medioInstance.slug= medioInstance.slug.replaceAll('[^a-z]','_')

            if (!medioInstance.save(flush: true)) {
                render view: 'create', model: [medioInstance: medioInstance]
                return
            }

            flash.message = message(code: 'default.created.message', args: [
                    message(code: 'libro.label', default: 'Medio'),
                    medioInstance.id
                ])
            redirect action: 'show', id: medioInstance.id
            break
        }
    }

    @Secured(["hasRole('ROLE_USER')"])
    def show() {
        def	user = springSecurityService.currentUser
        def medioInstance = params.id ? Medio.get(params.id) : Medio.findByResponsable(user)

        if (!medioInstance) {
            redirect action: 'create'
            return
        }

        if (medioInstance.responsable != user) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'medio.label', default: 'Medio'),
                    params.id
                ])
            redirect action: 'list'
            return
        }

        [medioInstance: medioInstance]
    }
    
    @Secured(["hasRole('ROLE_USER')"])
    def edit() {
        switch (request.method) {
        case 'GET':
            def medioInstance = Medio.get(params.id)
            if (!medioInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'medio.label', default: 'Medio'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            [medioInstance: medioInstance]
            break
        case 'POST':
            def medioInstance = Medio.get(params.id)
            if (!medioInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'medio.label', default: 'Medio'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (medioInstance.version > version) {
                    medioInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'medio.label', default: 'Medio')] as Object[],
								"Another user has updated this Medio while you were editing")
                    render view: 'edit', model: [medioInstance: medioInstance]
                    return
                }
            }

            medioInstance.properties = params
            medioInstance.slug= medioInstance.slug.replaceAll('[^a-z]','_')

            def user= springSecurityService.currentUser

            if (medioInstance.responsable != user || !medioInstance.save(flush: true)) {
                render view: 'edit', model: [medioInstance: medioInstance]
                return
            }

            flash.message = message(code: 'default.updated.message', args: [
                    message(code: 'medio.label', default: 'Medio'),
                    medioInstance.id
                ])
            redirect action: 'show', id: medioInstance.id
            break
        }
    }
  
    def test_getall() {
        //def props= Propuesta.getAll{it.epigrafe.keyword in ['firma','titulares','actualizacion','enlace']}
        def epi
        def libro= Libro.get(22)
        ['firma','titulares','actualizacion','enlace'].each{
            epi= Epigrafe.findByKeyword(it)
            libro.estilo.addAll(Propuesta.findAllByEpigrafe(epi))
        }
        [props: libro.estilo]
    }
    
    @Secured(["hasRole('ROLE_USER')"])
    def edit_1() {
        def user= springSecurityService.currentUser

        switch (request.method) {
        case 'GET':
            def medioInstance = Medio.findByResponsable(user)
            if (!medioInstance) {
                medioInstance = new Medio()
                medioInstance.responsable= user
                medioInstance.slug= user.username
                medioInstance.save()
            }

            if (!medioInstance.libro){
                def libroInstance= new Libro()
                def epi, estilo=[], etica=[]
                log.debug("Creando libro para medio: "+medioInstance.id)
                libroInstance.medio= medioInstance
                ['firma','titulares','actualizacion','enlace'].each{
                    epi= Epigrafe.findByKeyword(it)
                    estilo.addAll(Propuesta.findAllByEpigrafe(epi))
                }
                libroInstance.estilo= estilo
                ['fuentes','error','verificacion','plagio', 'redes'].each{
                    epi= Epigrafe.findByKeyword(it)
                    etica.addAll(Propuesta.findAllByEpigrafe(epi))
                }
                libroInstance.etica= etica
 
                //libroInstance.estilo= Propuesta.getAll{it.epigrafe.keyword in ['firma','titulares','actualizacion','enlace']}
                //libroInstance.etica= Propuesta.getAll{it.epigrafe.keyword in ['fuentes','error','verificacion','plagio', 'redes']}
                medioInstance.libro= libroInstance
                libroInstance.save()
                medioInstance.save()
            }
            
            [medioInstance: medioInstance]
            break
        case 'POST':
            def medioInstance = Medio.get(params.id)
            if (!medioInstance) {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'medio.label', default: 'Medio'),
                        params.id
                    ])
                redirect action: 'list'
                return
            }

            if (params.version) {
                def version = params.version.toLong()
                if (medioInstance.version > version) {
                    medioInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [
                            message(code: 'medio.label', default: 'Medio')] as Object[],
								"Another user has updated this Medio while you were editing")
                    render view: 'edit_1', model: [medioInstance: medioInstance]
                    return
                }
            }

            medioInstance.properties = params
            /*
            def f = request.getFile('logo')
            if (!f.empty) {
            f.transferTo(new File('/some/local/dir/myfile.txt'))
            return
            }
             */
   
            //medioInstance.slug= medioInstance.slug.replaceAll('[^a-z]','_')

            if (medioInstance.responsable != user || !medioInstance.save(flush: true)) {
                render view: 'edit_1', model: [medioInstance: medioInstance]
                return
            }

            /*
            flash.message = message(code: 'default.updated.message', args: [
            message(code: 'libro.label', default: 'Libro'),
            libroInstance.id
            ])
             */
            flash.message= "El Libro de estilo de ${medioInstance.nombre} se ha actualizado"
            
            redirect controller: 'libro', action: 'edit_2'
            break
        }
    }
        
    @Secured(["hasRole('ROLE_ADMIN')"])
    def delete() {
        def medioInstance = Medio.get(params.id)
        if (!medioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'medio.label', default: 'Medio'),
                    params.id
                ])
            redirect action: 'list'
            return
        }

        try {
            medioInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [
                    message(code: 'medio.label', default: 'Medio'),
                    params.id
                ])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [
                    message(code: 'medio.label', default: 'Medio'),
                    params.id
                ])
            redirect action: 'show', id: params.id
        }
    }
}
