package com.librodeestilo

import org.springframework.dao.DataIntegrityViolationException

class PropuestaController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [propuestaInstanceList: Propuesta.list(params), propuestaInstanceTotal: Propuesta.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[propuestaInstance: new Propuesta(params)]
			break
		case 'POST':
	        def propuestaInstance = new Propuesta(params)
	        if (!propuestaInstance.save(flush: true)) {
	            render view: 'create', model: [propuestaInstance: propuestaInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), propuestaInstance.id])
	        redirect action: 'show', id: propuestaInstance.id
			break
		}
    }

    def show() {
        def propuestaInstance = Propuesta.get(params.id)
        if (!propuestaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
            redirect action: 'list'
            return
        }

        [propuestaInstance: propuestaInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def propuestaInstance = Propuesta.get(params.id)
	        if (!propuestaInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [propuestaInstance: propuestaInstance]
			break
		case 'POST':
	        def propuestaInstance = Propuesta.get(params.id)
	        if (!propuestaInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (propuestaInstance.version > version) {
	                propuestaInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'propuesta.label', default: 'Propuesta')] as Object[],
	                          "Another user has updated this Propuesta while you were editing")
	                render view: 'edit', model: [propuestaInstance: propuestaInstance]
	                return
	            }
	        }

	        propuestaInstance.properties = params

	        if (!propuestaInstance.save(flush: true)) {
	            render view: 'edit', model: [propuestaInstance: propuestaInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), propuestaInstance.id])
	        redirect action: 'show', id: propuestaInstance.id
			break
		}
    }

    def delete() {
        def propuestaInstance = Propuesta.get(params.id)
        if (!propuestaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
            redirect action: 'list'
            return
        }

        try {
            propuestaInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'propuesta.label', default: 'Propuesta'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
