package com.librodeestilo

import org.springframework.dao.DataIntegrityViolationException


class QAController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }
	
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [QAInstanceList: QA.list(params), QAInstanceTotal: QA.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[QAInstance: new QA(params)]
			break
		case 'POST':
	        def QAInstance = new QA(params)
	        if (!QAInstance.save(flush: true)) {
	            render view: 'create', model: [QAInstance: QAInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'QA.label', default: 'QA'), QAInstance.id])
	        redirect action: 'show', id: QAInstance.id
			break
		}
    }

    def show() {
        def QAInstance = QA.get(params.id)
        if (!QAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
            redirect action: 'list'
            return
        }

        [QAInstance: QAInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def QAInstance = QA.get(params.id)
	        if (!QAInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [QAInstance: QAInstance]
			break
		case 'POST':
	        def QAInstance = QA.get(params.id)
	        if (!QAInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (QAInstance.version > version) {
	                QAInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'QA.label', default: 'QA')] as Object[],
	                          "Another user has updated this QA while you were editing")
	                render view: 'edit', model: [QAInstance: QAInstance]
	                return
	            }
	        }

	        QAInstance.properties = params

	        if (!QAInstance.save(flush: true)) {
	            render view: 'edit', model: [QAInstance: QAInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'QA.label', default: 'QA'), QAInstance.id])
	        redirect action: 'show', id: QAInstance.id
			break
		}
    }

    def delete() {
        def QAInstance = QA.get(params.id)
        if (!QAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
            redirect action: 'list'
            return
        }

        try {
            QAInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'QA.label', default: 'QA'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
