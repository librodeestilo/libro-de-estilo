package com.librodeestilo

import java.util.Date;

class Epigrafe implements Comparable {
	Integer orden
	String titulo
	String keyword
	String descripcion
	SortedSet propuestas

	static hasMany = [propuestas: Propuesta]
	static belongsTo = [master: Master]

	static mapping = {
		descripcion type:'text'
		propuestas(sort:'orden', order: "asc")
	}

	String toString(){
		return titulo
	}
	static constraints = { 
		orden()
		titulo(nullable:true) 
		descripcion(nullable:true) 
		}

	int compareTo(Object e){
		return orden.compareTo(e.orden)
	}
}
