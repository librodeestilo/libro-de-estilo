package com.librodeestilo

class Info implements Comparable {
	String orden
	String titulo
	String descripcion

	static mapping = { descripcion type:'text' }

	static constraints = {
		titulo nullable: true
		descripcion nullable:true
	}
	
	String toString(){
		return titulo
	}
	
	int compareTo(Object e){
		return orden.compareTo(e.orden)
	}
}
