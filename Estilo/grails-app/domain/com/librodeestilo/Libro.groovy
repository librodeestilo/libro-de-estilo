package com.librodeestilo

import java.util.Date;

import com.librodeestilo.auth.User

class Libro {
	String titulo
	String descripcion
	Boolean activo
	Medio medio
        Date dateCreated
        Date lastUpdated
	
	static hasMany = [propuestas: Propuesta, estilo: Propuesta, etica: Propuesta]
    
	static mapping = { descripcion type:'text' }

	static constraints = {
		titulo nullable: true
		descripcion nullable:true
		activo nullable:true
		medio nullable:true
                dateCreated nullable:true
                lastUpdated nullable:true
	}
	
	String toString(){
		return titulo
	}
}
