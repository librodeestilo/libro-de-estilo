package com.librodeestilo

class Master {
	String descripcion
	SortedSet epigrafes

	static hasMany = [epigrafes: Epigrafe]

	static mapping = {
		descripcion type:'text'
		epigrafes(sort:'orden', order: "asc")
	}

	static constraints = { descripcion nullable:true  }

	String toString(){
		return descripcion
	}
}
