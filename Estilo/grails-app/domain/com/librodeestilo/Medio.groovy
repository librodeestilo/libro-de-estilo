package com.librodeestilo

import com.librodeestilo.auth.User

class Medio{
    String nombre
    String slug
    String url
    String descripcion
    User responsable
    Libro libro
    byte[] logo

    static mapping = {
        descripcion type:'text'
        logo sqlType:'blob', maxSize: 1000000
    }

    String toString(){
        return nombre
    }
    static constraints = { 
        nombre(nullable:true)
        slug(nullable:true)
        url(nullable:true) 
        descripcion(nullable:true)
        responsable(nullable:true)
        libro(nullable:true)
        logo(nullable:true)
    }
}
