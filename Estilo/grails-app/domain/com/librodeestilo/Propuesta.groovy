package com.librodeestilo

class Propuesta implements Comparable {
	Integer orden
	Integer nivel
	Date fecha
	Boolean actual
	String contenido
	String ejemplos

	static hasMany = [preguntas: QA]
	static belongsTo = [epigrafe: Epigrafe]

	static mapping = {
		contenido type:'text'
		ejemplos type:'text'
		sort orden: "asc"
                tablePerHierarchy false
	}

	static constraints = {
		orden()
		nivel()
		fecha()
		actual()
		contenido()
		ejemplos(nullable:true)
	}

	String toString(){
		return contenido
	}
	
	int compareTo(Object e){
		return orden.compareTo(e.orden)
	}
}
