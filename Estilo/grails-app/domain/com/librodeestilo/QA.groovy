package com.librodeestilo

import java.util.Date;

class QA {
	Integer orden
	String pregunta
	String respuesta
	Date dateCreated
	Date lastUpdated
	
	static mapping = {respuesta type:'text' }
	static belongsTo = [propuesta: Propuesta]
	
	static constraints = {
		pregunta nullable:true
		respuesta nullable:true
	}
}
