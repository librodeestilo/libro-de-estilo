<%@ page import="com.librodeestilo.QA" %>



<div class="fieldcontain ${hasErrors(bean: QAInstance, field: 'pregunta', 'error')} ">
	<label for="pregunta">
		<g:message code="QA.pregunta.label" default="Pregunta" />
		
	</label>
	<g:textField name="pregunta" value="${QAInstance?.pregunta}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: QAInstance, field: 'respuesta', 'error')} ">
	<label for="respuesta">
		<g:message code="QA.respuesta.label" default="Respuesta" />
		
	</label>
	<g:textField name="respuesta" value="${QAInstance?.respuesta}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: QAInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="QA.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${QAInstance.orden}" required=""/>
</div>

