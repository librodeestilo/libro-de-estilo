
<%@ page import="com.librodeestilo.QA" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'QA.label', default: 'QA')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="pregunta" title="${message(code: 'QA.pregunta.label', default: 'Pregunta')}" />
						
							<g:sortableColumn property="respuesta" title="${message(code: 'QA.respuesta.label', default: 'Respuesta')}" />
						
							<g:sortableColumn property="orden" title="${message(code: 'QA.orden.label', default: 'Orden')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${QAInstanceList}" var="QAInstance">
						<tr>
						
							<td>${fieldValue(bean: QAInstance, field: "pregunta")}</td>
						
							<td>${fieldValue(bean: QAInstance, field: "respuesta")}</td>
						
							<td>${fieldValue(bean: QAInstance, field: "orden")}</td>
						
							<td class="link">
								<g:link action="show" id="${QAInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${QAInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
