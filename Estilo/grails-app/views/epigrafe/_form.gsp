<%@ page import="com.librodeestilo.Epigrafe" %>



<div class="fieldcontain ${hasErrors(bean: epigrafeInstance, field: 'contenido', 'error')} ">
	<label for="contenido">
		<g:message code="epigrafe.contenido.label" default="Contenido" />
		
	</label>
	<g:textField name="contenido" value="${epigrafeInstance?.contenido}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: epigrafeInstance, field: 'master', 'error')} required">
	<label for="master">
		<g:message code="epigrafe.master.label" default="Master" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="master" name="master.id" from="${com.librodeestilo.Master.list()}" optionKey="id" required="" value="${epigrafeInstance?.master?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: epigrafeInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="epigrafe.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${epigrafeInstance.orden}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: epigrafeInstance, field: 'recomendaciones', 'error')} ">
	<label for="recomendaciones">
		<g:message code="epigrafe.recomendaciones.label" default="Recomendaciones" />
		
	</label>
	<g:select name="recomendaciones" from="${com.librodeestilo.Propuesta.list()}" multiple="multiple" optionKey="id" size="5" value="${epigrafeInstance?.recomendaciones*.id}" class="many-to-many"/>
</div>

