<%@ page import="com.librodeestilo.Epigrafe"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'epigrafe.label', default: 'Epigrafe')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div class="row-fluid">

		<div class="span3">
			<div class="well">
				<ul class="nav nav-list">
					<li class="nav-header">
						${entityName}
					</li>
					<li><g:link class="list" action="list">
							<i class="icon-list"></i>
							<g:message code="default.list.label" args="[entityName]" />
						</g:link></li>
					<li><g:link class="create" action="create">
							<i class="icon-plus"></i>
							<g:message code="default.create.label" args="[entityName]" />
						</g:link></li>
				</ul>
			</div>
		</div>

		<div class="span9">

			<div class="page-header">
				<h1>
					<g:message code="default.edit.label" args="[entityName]" />
				</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${epigrafeInstance}">
				<bootstrap:alert class="alert-error">
					<ul>
						<g:eachError bean="${epigrafeInstance}" var="error">
							<li
								<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
									error="${error}" /></li>
						</g:eachError>
					</ul>
				</bootstrap:alert>
			</g:hasErrors>


			<g:form class="form-horizontal" action="edit"
				id="${epigrafeInstance?.id}">
				<g:hiddenField name="version" value="${epigrafeInstance?.version}" />
				<fieldset>
					<f:with bean="epigrafeInstance">
						<f:field property="master" />
						<f:field property="orden" />
						<f:field property="titulo" />
						<f:field property="keyword" />
						<f:field property="descripcion">
							<g:textArea name="${property}" value="${value}"
								class="input-block-level" style="height:300px;"></g:textArea>
						</f:field>
						<f:field property="propuestas" />

					</f:with>
					<div class="form-actions">
						<button type="submit" class="btn btn-primary">
							<i class="icon-ok icon-white"></i>
							<g:message code="default.button.update.label" default="Update" />
						</button>
						<button type="submit" class="btn btn-danger" name="_action_delete"
							formnovalidate>
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</fieldset>
			</g:form>

		</div>

	</div>
</body>
</html>
