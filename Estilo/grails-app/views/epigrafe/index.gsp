
<%@ page import="com.librodeestilo.Epigrafe" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'epigrafe.label', default: 'Epigrafe')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-epigrafe" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-epigrafe" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="contenido" title="${message(code: 'epigrafe.contenido.label', default: 'Contenido')}" />
					
						<th><g:message code="epigrafe.master.label" default="Master" /></th>
					
						<g:sortableColumn property="orden" title="${message(code: 'epigrafe.orden.label', default: 'Orden')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${epigrafeInstanceList}" status="i" var="epigrafeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${epigrafeInstance.id}">${fieldValue(bean: epigrafeInstance, field: "contenido")}</g:link></td>
					
						<td>${fieldValue(bean: epigrafeInstance, field: "master")}</td>
					
						<td>${fieldValue(bean: epigrafeInstance, field: "orden")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${epigrafeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
