<%@ page import="com.librodeestilo.Info" %>



<div class="fieldcontain ${hasErrors(bean: infoInstance, field: 'titulo', 'error')} ">
	<label for="titulo">
		<g:message code="info.titulo.label" default="Titulo" />
		
	</label>
	<g:textField name="titulo" value="${infoInstance?.titulo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: infoInstance, field: 'descripcion', 'error')} ">
	<label for="descripcion">
		<g:message code="info.descripcion.label" default="Descripcion" />
		
	</label>
	<g:textField name="descripcion" value="${infoInstance?.descripcion}"/>
</div>

