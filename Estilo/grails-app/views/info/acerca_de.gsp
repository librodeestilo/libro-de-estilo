
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html>
    <head>
        <meta name="layout" content="lde">
        <g:set var="entityName" value="${message(code: 'info.label', default: 'Info')}" />
        <title>Acerca de librodeestilo.com</title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span2">
            </div>

            <div class="span8">
                <div class="headline">
                <h1>Acerca de librodeestilo.com</h1>
                </div>
                
                <h2>
                    Un proyecto de la Universidad del País Vasco / Euskal Herriko Unibertsitatea  creado por y para periodistas
                </h2>

                <p class="first">
                    El presente libro de estilo tiene una doble función: ser una herramienta y ser un contrato. 
                    La herramienta no es otra que el <strong>método periodístico</strong> de obtención y expresión de la información, puesto a disposición de todas aquellas personas 
                    (Quién) que desean informar sobre cualquier tema (Qué), en cualquier momento 
                    (Cuándo), desde cualquier lugar y medio (Dónde), por cualquier razón (Por qué).
                </p>
                <p>
                    Sirviéndonos del esquema narrativo de la Pirámide Invertida, base expresiva de la 
                    práctica periodística, este libro de estilo se ocupa de la pregunta Cómo, cuál es el 
                    método para elaborar una información.
                </p>
                <p>
                    Por otro lado, es un <strong>contrato</strong> en el que el medio elige libre y voluntariamente 
                    la parte del método que va a cumplir, esto es, qué límites va a tener su práctica 
                    informativa, y los hace públicos, de forma que su audiencia es conocedora de 
                    dicha práctica y puede exigir (solo) en consecuencia.
                </p>
                <p>  
                    El libro de estilo tiene una <strong>estructura</strong> triangular y dinámica. Es triangular 
                    porque se asienta en tres ejes: las propuestas que tratan de sintetizar el método; 
                    la participación abierta a todos los usuarios en relación para completar dicho 
                    método; y las preguntas y respuestas que surgen sobre el mismo, y que generan 
                    una reflexión. Y es una estructura dinámica porque los tres ejes se relacionan 
                    entre sí de manera constante y biunívoca, y porque es un proyecto que tiene la 
                    vocación de responder a las necesidades del momento. Que está en progresión 
                    constante, abierto...
                </p>
                <p>
                    El <strong>equipo</strong> de trabajo que ha puesto en marcha este libro de estilo participa de 
                    esta estructura triangular y dinámica. Está formado por un grupo de periodistas 
                    en activo, con una larga trayectoria profesional, y un grupo de profesores de la 
                    Universidad del País Vasco UPV/EHU. A ambos grupos se suma un tercero, el 
                    conjunto de usuarios que, a través de su participación, completará el libro de 
                    estilo.
                </p>
            </div>
            <div class="span2"></div>

        </div>
    </div>
</body>
</html>
