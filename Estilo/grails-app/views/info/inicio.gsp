
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html>
    <head>
        <meta name="layout" content="lde">
        <g:set var="entityName" value="${message(code: 'info.label', default: 'Info')}" />
        <title>${infoInstance?.titulo}</title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span2">

            </div>

            <div class="span8">
                <div class="cab">Libro de estilo</div>

                <h2>Una herramienta colaborativa para medios de comunicación...</h2>

                <p>
      ...creada por periodistas, profesores de la UPV/EHU y técnicos para generar un libro de estilo periodístico a medida partiendo de los recursos existentes. <a href="/info/acerca_de/">Más información</a>.
                </p>
            </div>
            
            <div class="span2">

            </div>

        </div>
    </div>
</body>
</html>
