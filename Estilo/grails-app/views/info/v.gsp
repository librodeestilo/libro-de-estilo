
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'info.label', default: 'Info')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
	
			</div>
			
			<div class="span9">

				<div class="page-header">
<g:if test="${infoInstance?.titulo}">
						
							<h2><g:fieldValue bean="${infoInstance}" field="titulo"/></h2>
						
					</g:if>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
								
					<g:if test="${infoInstance?.descripcion}">
							<markdown:renderHtml text="${infoInstance.descripcion}" />						
					</g:if>

			</div>

		</div>
	</body>
</html>
