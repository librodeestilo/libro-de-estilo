<%@ page
	import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes"%>
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="Libro de estilo" /></title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="google-site-verification" content="60IYiIu9pIXrS6b3b60SofoPhRpVZUT-ELFnncIVheo" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

<r:require modules="bootstrap" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir: 'images', file: 'favicon.ico')}"
	type="image/x-icon">
<link rel="apple-touch-icon"
	href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="114x114"
	href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

<g:layoutHead />
<r:layoutResources />

<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'libro.css')}" type="text/css">
<link href="data:image/x-icon;base64,AAABAAEAEBACAAAAAACwAAAAFgAAACgAAAAQAAAAIAAAAAEAAQAAAAAAQAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAA/4QAAAAAAABABAAAY4wAAF10AABBBAAAQQQAAEEEAABBBAAAQQQAAEEEAABBBAAAI4gAABxwAAAAAAAAAAAAAAAAAAD//wAAv/sAAJxzAACiiwAAvvsAAL77AAC++wAAvvsAAL77AAC++wAAvvsAANx3AADjjwAA//8AAP//AAD//wAA" rel="icon" type="image/x-icon" />
</head>

<body style="font-family: 'Open Sans'; ">
	<nav class="navbar navbar-default">
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- 
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
				-->	
				<a class="cuerpo1 brand" style="color:#000;font-size:1.7em;"  href="${createLink(uri: '/')}">Libro de estilo</a>

				<!--  <div class="nav-collapse"> -->
					<ul class="nav">
						<g:each var="c"
							in="${Info.list().sort()}">
							<li><g:link class="cuerpo5" controller="info" action="v" id="${c.id}">
									${c.titulo}
								</g:link></li>
						</g:each>
						<li><g:link controller='libro' action='pdf'>PDF</g:link></li>
						<li><sec:ifNotLoggedIn>
								<g:link class="pull-right" controller='login' action='auth'>Entrar</g:link>
							</sec:ifNotLoggedIn>
							
							<sec:ifLoggedIn>
								<g:link class="pull-right" controller='libro' action='edit'>Editar libro</g:link>
							</sec:ifLoggedIn>
							
							
						</li>
						
							<sec:ifLoggedIn>
							<li>
								<g:link class="pull-right" controller='logout'>Salir</g:link>
							</li>
							</sec:ifLoggedIn>
						
						
						
					</ul>
				<!--  </div>-->

			</div>
		</div>
	</nav>

	<g:layoutBody />
	<r:layoutResources />

<footer>
<div class="footer">
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Reconocimiento-NoComercial 4.0</a>.
<br/><a href="mailto:info@librodeestilo.com">info@librodeestilo.com</a>
</div>
</footer>
</body>
</html>
