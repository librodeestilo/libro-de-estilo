<%@ page
import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes"%>
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><g:layoutTitle default="Libro de estilo" /></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="google-site-verification" content="60IYiIu9pIXrS6b3b60SofoPhRpVZUT-ELFnncIVheo" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
                        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->

            <r:require modules="bootstrap" />
<!-- Le fav and touch icons -->
    <link rel="shortcut icon"
    href="${resource(dir: 'images', file: 'favicon.ico')}"
    type="image/x-icon">
    <link rel="apple-touch-icon"
    href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114"
    href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

    <g:layoutHead />
    <r:layoutResources />

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'lde.css', absolute:'true')}" type="text/css">
    <link href="data:image/x-icon;base64,AAABAAEAEBACAAAAAACwAAAAFgAAACgAAAAQAAAAIAAAAAEAAQAAAAAAQAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAA/4QAAAAAAABABAAAY4wAAF10AABBBAAAQQQAAEEEAABBBAAAQQQAAEEEAABBBAAAI4gAABxwAAAAAAAAAAAAAAAAAAD//wAAv/sAAJxzAACiiwAAvvsAAL77AAC++wAAvvsAAL77AAC++wAAvvsAANx3AADjjwAA//8AAP//AAD//wAA" rel="icon" type="image/x-icon" />
</head>

<body>
    <g:layoutBody />
<r:layoutResources />

</body>
</html>
