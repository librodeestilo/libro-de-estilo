<%@ page
import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes"%>
<%@ page import="com.librodeestilo.Info" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><g:layoutTitle default="Libro de estilo" /></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="google-site-verification" content="60IYiIu9pIXrS6b3b60SofoPhRpVZUT-ELFnncIVheo" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
                        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->

    <r:require modules="bootstrap" />

<!-- Le fav and touch icons -->
    <link rel="shortcut icon"
    href="${resource(dir: 'images', file: 'favicon.ico')}"
    type="image/x-icon">
    <link rel="apple-touch-icon"
    href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114"
    href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

    <g:layoutHead />
    <r:layoutResources />

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'lde.css')}" type="text/css">
    <link href="data:image/x-icon;base64,AAABAAEAEBACAAAAAACwAAAAFgAAACgAAAAQAAAAIAAAAAEAAQAAAAAAQAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAA/4QAAAAAAABABAAAY4wAAF10AABBBAAAQQQAAEEEAABBBAAAQQQAAEEEAABBBAAAI4gAABxwAAAAAAAAAAAAAAAAAAD//wAAv/sAAJxzAACiiwAAvvsAAL77AAC++wAAvvsAAL77AAC++wAAvvsAANx3AADjjwAA//8AAP//AAD//wAA" rel="icon" type="image/x-icon" />
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="">
            <div class="container-fluid">
                    <!-- 
                            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                            </a>
                    -->	
                <a class="brand" href="${createLink(uri: '/')}">
                    <img src="${resource(dir: 'images', file: 'lde-noborder.png')}" alt="Libro de estilo"/>
                </a>

                                <!--  <div class="nav-collapse"> -->
                <ul class="nav">
                    <li><g:link controller="info" action="inicio">
                            Inicio
                        </g:link></li>
                    <li><g:link controller="libro" action="recursos">
                            Recursos
                        </g:link></li>
                    <li><g:link controller="info" action="acerca_de">
                            Acerca de
                        </g:link></li>
                    <li>
                    <sec:ifNotLoggedIn>
                        <g:link controller='login' action='auth'>
                            Acceder
                        </g:link>
                    </sec:ifNotLoggedIn>

                    <sec:ifLoggedIn>
                        <g:link class="user" controller='libro' action='base' params="[usuario: sec.loggedInUserInfo(field: 'username')]">
                            <sec:loggedInUserInfo field="username"/>
                        </g:link>
                    </sec:ifLoggedIn>
                    </li>

                    <sec:ifLoggedIn>
                        <li>
                            <g:link class="user" controller='logout'>Cerrar sesión</g:link>
                            </li>
                        </sec:ifLoggedIn>
                    </ul>
                </div>
                <sec:ifLoggedIn>
                    <div class="container-fluid" style="border-bottom:1px solid #000">
                        <ul class="nav subnav" style="padding-left:8rem">
                                <li><g:link controller="medio" action="edit_1">
                                    1.- Ficha
                                </g:link></li>
                            <li><g:link controller="libro" action="edit_2">
                                    2.- Introducción
                                </g:link></li>
                            <li><g:link controller="libro" action="edit_3">
                                    3.- Estilo
                                </g:link></li>
                            <li><g:link controller="libro" action="edit_4">
                                    4.- Ética
                                </g:link></li>
                            <!--
                                <li><g:link controller="libro" action="edit" params="[seccion:'misc']">
                                        5.- Miscelánea
                                </g:link></li>
                            -->
                    </ul>
                </div>
            </sec:ifLoggedIn>
            <!--  </div>-->
        </div>
    </div>
</nav>


<g:layoutBody />
<r:layoutResources />

<footer>
    <div class="footer">
        <table style="margin: auto">
            <tr>
                <td>
                    <a class="brand" href="${createLink(uri: 'http://www.ehu.es')}">
         <img src="${resource(dir: 'images', file: 'UPV-EHU.png')}" alt="UPV-EHU"/>
                    </a>
                </td>
                <td>
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons</a>.
                </td>
            </tr>
        </table>
    </div>
</footer>
</body>
</html>
