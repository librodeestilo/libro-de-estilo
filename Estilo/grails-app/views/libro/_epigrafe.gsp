<h5 class="epi" data-toggle="collapse" data-target="#${it.keyword}">
    ${it.titulo}
</h5>
<div id="${it.keyword}" class="collapse out">
    <g:if test="${it.propuestas}">
        <markdown:renderHtml text="${it.descripcion}" />
        <ul>
            <g:each status="j" in="${it.propuestas.sort()}" var="r">
                <li>
                <markdown:renderHtml text="${r.contenido}" />
                <g:if test="${r.ejemplos}">
                    <div class="ejemplo">
                        <markdown:renderHtml text="${r.ejemplos}" />
                    </div>
                </g:if>

                </li>
            </g:each>
        </ul>
    </g:if>
</div>   
