<%@ page import="com.librodeestilo.Libro" %>



<div class="fieldcontain ${hasErrors(bean: libroInstance, field: 'descripcion', 'error')} ">
	<label for="descripcion">
		<g:message code="libro.descripcion.label" default="Descripcion" />
		
	</label>
	<g:textField name="descripcion" value="${libroInstance?.descripcion}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: libroInstance, field: 'recomendaciones', 'error')} ">
	<label for="recomendaciones">
		<g:message code="libro.recomendaciones.label" default="Recomendaciones" />
		
	</label>
	<g:select name="recomendaciones" from="${com.librodeestilo.Propuesta.list()}" multiple="multiple" optionKey="id" size="5" value="${libroInstance?.recomendaciones*.id}" class="many-to-many"/>
</div>

