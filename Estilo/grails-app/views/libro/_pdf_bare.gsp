<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta name="layout" content="bootstrap" />
<g:set var="entityName"
	value="${message(code: 'libro.label', default: 'Libro de estilo')}" />
<title>Libro de estilo</title>
<style type="text/css">
@page {
	size: 210mm 297mm;
}
li {
    page-break-inside: avoid;
  }
</style>
</head>
<body>
	<div class="row-fluid">

		<div class="span1"></div>

		<div class="span10">

			<div style="page-break-after: always;">

				<g:if test="${medio}">
					<h2>
						${medio.nombre}
					</h2>
					${medio.descripcion}
				</g:if>
				<g:else>
					<h2>Libro de estilo</h2>
					<markdown:renderHtml text="${master.descripcion}" />
				</g:else>
			</div>

			<g:each status="i" in="${master.epigrafes}" var="e">
				<g:if test="${e.propuestas}">

					<div class="epigrafe">
					
					<div style="page-break-inside: avoid;">
						<h2>
							${e.titulo}
						</h2>

						
						<markdown:renderHtml text="${e.descripcion}" />
					</div>
						<ul>
							<g:if test="${libroInstance}">
								<g:each
									in="${libroInstance.propuestas.findAll{it.epigrafe==e}.sort()}"
									var="r">
									<li><markdown:renderHtml text="${r.contenido}" /></li>
								</g:each>
							</g:if>
							<g:else>
								<g:each status="j" in="${e.propuestas.sort()}" var="r">
									<li><markdown:renderHtml text="${r.contenido}" /></li>

								</g:each>
							</g:else>
						</ul>
					</div>
				</g:if>
			</g:each>

		</div>
	</div>
</body>
</html>
