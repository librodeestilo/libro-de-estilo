<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<%@ page import="com.librodeestilo.Epigrafe"%>
<!doctype html>
<html>
    <head>
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro de estilo')}" />
        <title>Libro de estilo</title>
    </head>
    <body>
        
        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">

                <g:if test="${libroInstance}">
                    <h1>Hola, <sec:loggedInUserInfo field="username"/></h1>
                    <h2>
                        Este es tu libro de estilo. Pincha en 'Editar' para editar su contenido o accede a la versión web (html) y 
                        descargable (pdf).
                    </h2>                
                <div>
                    <div class="row">
                        <div class="span4">
                        <img src="${resource(dir: 'images', file: 'lde-triptico.png')}" alt="Libro de estilo"/>
                    </div>
                    <div class="span8" style="margin:auto">
                    <ul class="opciones">
                        <g:set var="link" value="${grailsApplication.config.grails.serverURL}"/>
                        <g:set var="slug" value="${libroInstance.medio.slug}"/>
                        <g:set var="nombre" value="${libroInstance.medio.nombre}"/>
                        <g:set var="modificacion" value="${libroInstance.lastUpdated}"/>
                        <h3>${nombre}</h3>
                        <g:if test="${modificacion}">
                            <h4><g:formatDate format="dd 'de' MMMMM 'de' yyyy"date="${libroInstance?.lastUpdated}" style="LONG"/></h4>
                        </g:if>
                        <li><g:link controller="medio" action="edit_1">EDITAR</g:link> |</li>
                        <li> <g:link controller="libro" action="view_html" params="[slug:slug, format:'pdf']">PDF</g:link> |</li>
                        <li> <g:link controller="libro" action="view_html" params="[slug:slug, format: 'html']">HTML</g:link></li>				
                        <!--
                        <li> <g:link controller="libro" action="view_pdf" params="[slug:slug]">PDF</g:link> |</li>
                        <li> <g:link controller="libro" action="view_html" params="[slug:slug]">HTML</g:link></li>
                        -->
                        </ul>
                    </div>
                    </div>
                </div>

                </g:if>
                <g:else>
                    <h2>
                        ¿Listo para empezar? Te hemos preparado un borrador de libro de estilo  con una serie de propuestas por defecto.
                        <g:link controller="medio" action="edit_1"> Entra, edita y publica.</g:link>
                    </h2>
                </g:else>   
            </div>
            <div class="span2">

            </div>

        </div>
    </body>
</html>
