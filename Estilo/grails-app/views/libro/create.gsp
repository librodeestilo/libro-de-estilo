<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>

<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'libro.label', default: 'Libro')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>

	<div class="row-fluid">

		<div class="span3">
			<div class="well">
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<ul class="nav nav-list">
					<li class="nav-header">
						${entityName}
					</li>
					<li><g:link class="list" action="list">
							<i class="icon-list"></i>
							<g:message code="default.list.label" args="[entityName]" />
						</g:link></li>
					<li class="active"><g:link class="create" action="create">
							<i class="icon-plus icon-white"></i>
							<g:message code="default.create.label" args="[entityName]" />
						</g:link></li>
				</ul>
				</sec:ifAnyGranted>
			</div>
		</div>

		<div class="span9">

			<div class="page-header">
				<h1>
					<g:message code="default.create.label" args="[entityName]" />
				</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${libroInstance}">
				<bootstrap:alert class="alert-error">
					<ul>
						<g:eachError bean="${libroInstance}" var="error">
							<li
								<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
									error="${error}" /></li>
						</g:eachError>
					</ul>
				</bootstrap:alert>
			</g:hasErrors>


			<g:form class="form-horizontal" action="create">
				<fieldset>
					<f:field bean="libroInstance" property="titulo" />
					<f:field bean="libroInstance"property="descripcion">
					<g:textArea name="${property}" value="${value}" class="input-block-level" style="height:300px;"></g:textArea>
					</f:field>

					<div class="bs-example bs-example-tabs">
						<ul id="myTab" class="nav nav-tabs">
							<g:each status="i" in="${master.epigrafes}" var="e">
								<li ${i==0?'class="active"':''}><a
									href="#${e.keyword}" data-toggle="tab"> ${e.titulo}
								</a></li>
							</g:each>
						</ul>
						<div id="myTabContent" class="tab-content">
							<g:each status="i" in="${master.epigrafes}" var="e">

								<div class="tab-pane fade in ${i==0?'active':''}"
									id="${e.keyword}">
									<ul>

										<g:each in="${e.propuestas.sort()}" var="r">
											<li><g:checkBox name="propuesta.${r.id}"
													value="${libroInstance.propuestas?.contains(r)}" /> <markdown:renderHtml
													text="${r.contenido}" /></li>
										</g:each>
									</ul>
								</div>
							</g:each>
						</div>
					</div>


					<div class="form-actions">
						<button type="submit" class="btn btn-primary">
							<i class="icon-ok icon-white"></i>
							<g:message code="default.button.create.label" default="Create" />
						</button>
					</div>
				</fieldset>
			</g:form>


		</div>

	</div>
</body>
</html>
