<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
    <head>
        <meta name="layout" content="bootstrap">
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span3">
                <div class="well">

                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <ul class="nav nav-list">
                            <li class="nav-header">
                                ${entityName}
                            </li>
                            <li><g:link class="list" action="list">
                                    <i class="icon-list"></i>
                                    <g:message code="default.list.label" args="[entityName]" />
                                </g:link></li>
                            <li><g:link class="create" action="create">
                                    <i class="icon-plus"></i>
                                    <g:message code="default.create.label" args="[entityName]" />
                                </g:link></li>
                        </ul>
                    </sec:ifAnyGranted>

                    <g:link controller="medio" action="edit"
                    id="${libroInstance.medio.id}">
                        Editar medio: ${libroInstance.medio.nombre}
                    </g:link>

                </div>
            </div>

            <div class="span9">



                <g:if test="${flash.message}">
                    <bootstrap:alert class="alert-info">
                        ${flash.message}
                    </bootstrap:alert>
                </g:if>

                <g:hasErrors bean="${libroInstance}">
                    <bootstrap:alert class="alert-error">
                        <ul>
                            <g:eachError bean="${libroInstance}" var="error">
                                <li
                                <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}" /></li>
                            </g:eachError>
                        </ul>
                    </bootstrap:alert>
                </g:hasErrors>

                <div>
                    <ul>
                        <g:set var="link" value="${grailsApplication.config.grails.serverURL}"/>
                        <g:set var="slug" value="${libroInstance.medio.slug}"/>
                        <li><a href="${link}/libro/${slug?:'slug'}.pdf">${link}/libro/${slug?:'slug'}.pdf</a></li>
                        <li><a href="${link}/libro/${slug?:'slug'}.html">${link}/libro/${slug?:'slug'}.html</a></li>				
                    </ul>
                </div>


                <g:form class="form-horizontal" action="edit"
                id="${libroInstance?.id}">
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-ok icon-white"></i>
                            <g:message code="default.button.update.label" default="Update" />
                        </button>

                    </div>
                    <g:hiddenField name="version" value="${libroInstance?.version}" />

                    <div class="bs-example bs-example-tabs">
                        <ul id="myTab" class="nav nav-tabs">
                            <g:each status="i" in="${master.epigrafes}" var="e">
                                <li ${i==0?'class="active"':''}><a href="#${e.keyword}"
                                                                   data-toggle="tab"> ${e.titulo}
                                </a></li>
                            </g:each>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <g:each status="i" in="${master.epigrafes}" var="e">

                            <div class="tab-pane fade in ${i==0?'active':''}"
                                 id="${e.keyword}">
                                <g:if test="${i==0}">
                                    <g:textArea name="descripcion" value="${libroInstance.descripcion}" class="input-block-level" style="height:300px;"></g:textArea>
                                </g:if>
                                <g:else>
                                <ul>
                                    <g:each in="${e.propuestas.sort()}" var="r">
                                        <li><g:checkBox name="propuesta.${r.id}"
                                            value="${libroInstance.propuestas?.contains(r)}" /> ${r.contenido}</li>
                                        </g:each>
                                </ul>
                                </g:else>
                            </div>
                        </g:each>
                    </div>
                </div>
                </fieldset>

                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <button class="btn btn-danger" type="submit" name="_action_delete">
                        <i class="icon-trash icon-white"></i>
                        <g:message code="default.button.delete.label" default="Delete" />
                    </button>
                </sec:ifAnyGranted>

            </g:form>

        </div>

    </div>
</body>
</html>
