<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
    <head>
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span2">
            </div>

            <div class="span8">

                <g:if test="${flash.message}">
                    <bootstrap:alert class="alert-info">
                        ${flash.message}
                    </bootstrap:alert>
                </g:if>

                <g:hasErrors bean="${libroInstance}">
                    <bootstrap:alert class="alert-error">
                        <ul>
                            <g:eachError bean="${libroInstance}" var="error">
                                <li
                                <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}" /></li>
                            </g:eachError>
                        </ul>
                    </bootstrap:alert>
                </g:hasErrors>

                
                 <div class="num">2</div>

                <h1>Introducción</h1>

                <hr/>

                <h2>Este el texto que presenta el libro de estilo del medio a los lectores y expone 
                    la motivación del mismo.</h2>

                <g:form class="form" action="edit_2"
                id="${libroInstance?.id}">
                    <g:hiddenField name="version" value="${libroInstance?.version}" />

                                <g:textArea name="descripcion" value="${libroInstance.descripcion}"
                                    class="input-block-level" style="height:300px;">
                                </g:textArea>
                   
                                <g:submitButton class="btn btn-block" name="actualizar" value="Enviar" />

            </g:form>

        </div>

    </div>
</body>
</html>
