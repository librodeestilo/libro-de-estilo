<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<%@ page import="com.librodeestilo.Epigrafe"%>

<!doctype html>
<html>
    <head>
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span2">
            </div>

            <div class="span8">

                <g:if test="${flash.message}">
                    <bootstrap:alert class="alert-info">
                        ${flash.message}
                    </bootstrap:alert>
                </g:if>

                <g:hasErrors bean="${libroInstance}">
                    <bootstrap:alert class="alert-error">
                        <ul>
                            <g:eachError bean="${libroInstance}" var="error">
                                <li
                                <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                error="${error}" /></li>
                            </g:eachError>
                        </ul>
                    </bootstrap:alert>
                </g:hasErrors>

                <div class="num">4</div>

                <h1>Ética</h1>

                <hr/>

                <h2>En cada sección hay una serie de epígrafes y propuestas para configurar tu libro de estilo. Por defecto aparecen todas seleccionadas. Deselecciona las que no te interesen y añade las que consideres oportunas. Aquí se incluyen las normas relacionadas con la ética periodística</h2>
                
                <g:form action="edit_4" id="${libroInstance?.id}">
                    <g:hiddenField name="version" value="${libroInstance?.version}" />
                                   
                        <g:each status="i" in="${['fuentes','error','verificacion','plagio', 'redes']}" var="k">
                            <g:set var="e" value="${Epigrafe.findByKeyword(k)}"/>
                                <div class="epi-neg">${e.titulo}</div>
                                    <p>${e.descripcion}</p>
                                <div class="epi-neg">Propuestas</div>
                                    <ul class="propuestas">
                                        <g:each in="${e.propuestas.sort()}" var="r">
                                            <li><g:checkBox name="propuesta.${r.id}"
                                                value="${libroInstance.etica?.contains(r)}" /> ${r.contenido}</li>
                                        </g:each>
                                    </ul>
                               
                        </g:each>
                    
                                <g:submitButton class="btn btn-block" name="actualizar" value="Enviar" />

            </g:form>

        </div>
        <div class="span2">
        </div>

    </div>
</body>
</html>
