<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<g:set var="entityName"
	value="${message(code: 'libro.label', default: 'Libro de estilo')}" />
<title>Libro de estilo</title>
</head>
<body>
	<div class="row-fluid">

		<div class="span1"></div>

		<div class="span10">

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<div class="bs-example bs-example-tabs">
				<ul id="myTab" class="nav nav-tabs">
					<g:each status="i" in="${master.epigrafes}" var="e">
						<li ${i==0?'class="active"':''}><a href="#${e.keyword}"
							data-toggle="tab"> ${e.titulo}
						</a></li>
					</g:each>
				</ul>
				<div id="myTabContent" class="tab-content">

					<g:each status="i" in="${master.epigrafes}" var="e">

						<div class="tab-pane fade in ${i==0?'active':''}"
							id="${e.keyword}">

							<g:if test="${e.propuestas}">

								<markdown:renderHtml text="${e.descripcion}" />

								<g:if test="${!libroInstance}">
								<h4 style="color: #777">Propuestas</h4>
								</g:if>

								<ul>
									<g:if test="${libroInstance}">
										<g:each
											in="${libroInstance.propuestas.findAll{it.epigrafe==e}.sort()}"
											var="r">
											<li><markdown:renderHtml text="${r.contenido}" /></li>
										</g:each>
									</g:if>
									<g:else>
										<g:each status="j" in="${e.propuestas.sort()}" var="r">
											<li>
												<markdown:renderHtml text="${r.contenido}" />
												<g:if test="${r.ejemplos}">
													<div id="p${i}-${j}" class="ejemplo">
														<markdown:renderHtml text="${r.ejemplos}" />
													</div>
												</g:if>

											</li>
										</g:each>
									</g:else>
								</ul>
							</g:if>
							<g:else>
								<g:if test="${libroInstance}">

									<h2>
										${libroInstance.titulo}
									</h2>

									<markdown:renderHtml text="${libroInstance.descripcion}" />
								</g:if>
								<g:else>
									<markdown:renderHtml text="${master.descripcion}" />

								</g:else>


							</g:else>
						</div>
					</g:each>
				</div>
			</div>

		</div>

	</div>
	<script>
		$(document).ready(
				function() {
					// show active tab on reload
					if (location.hash !== '') {
						$('a[href="' + location.hash + '"]').tab('show');
					}

					// remember the hash in the URL without jumping
					$('a[data-toggle="tab"]').on(
							'shown.bs.tab',
							function(e) {
								if (history.pushState) {
									history.pushState(null, null, '#'
											+ $(e.target).attr('href')
													.substr(1));
								} else {
									location.hash = '#'
											+ $(e.target).attr('href')
													.substr(1);
								}
							});
					$('body').scrollTop(0);
				});
	</script>
</body>
</html>
