<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<%@ page import="com.librodeestilo.Epigrafe"%>
<!doctype html>
<html>
    <head>
        <meta name="layout" content="lde" />
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro de estilo')}" />
        <title>Libro de estilo</title>
    </head>
    <body>
        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">
                <div class="headline">
                <h1>Recursos</h1>
                </div>

                <h2>Consulta la muestra de normas propuestas y úsalas como base de tu libro de estilo</h2>

                <h3>
                    Hay tres secciones (introducción, estilo y ética) que constan de varios epígrafes. 
                    Pincha en el título de cada sección para desplegar los distintos apartados y ver las propuestas de cada uno 
                    Una vez dado de alta como usuario, podrás incluir dichos elementos y editarlos en tu propio libro de estilo.
                </h3>

                <div class="sec">Introducción</div>

                <h4 class="sec">Estilo</h4>

                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('firma')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('titulares')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('actualizacion')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('enlace')}" />

                <div class="sec">
                    Ética
                </div>

                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('fuentes')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('error')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('verificacion')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('plagio')}" />
                <g:render template="epigrafe" bean="${Epigrafe.findByKeyword('redes')}" />

            </div>
            <div class="span2">

            </div>

        </div>
    </body>
</html>
