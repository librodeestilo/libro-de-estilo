<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
    <head>
        <meta name="layout" content="bootstrap">
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span3">
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <div class="well">
                        <ul class="nav nav-list">
                            <li class="nav-header">
                                ${entityName}
                            </li>
                            <li><g:link class="list" action="list">
                                    <i class="icon-list"></i>
                                    <g:message code="default.list.label" args="[entityName]" />
                                </g:link></li>
                            <li><g:link class="create" action="create">
                                    <i class="icon-plus"></i>
                                    <g:message code="default.create.label" args="[entityName]" />
                                </g:link></li>
                        </ul>
                    </div>
                </sec:ifAnyGranted>
            </div>

            <div class="span9">

                <g:if test="${flash.message}">
                    <bootstrap:alert class="alert-info">
                        ${flash.message}
                    </bootstrap:alert>
                </g:if>

                <g:if test="${libroInstance?.descripcion}">

                    <h2>
                        <g:fieldValue bean="${libroInstance}" field="titulo" />
                    </h2>

                </g:if>

                <g:if test="${libroInstance?.descripcion}">

                    <h2>
                        <g:fieldValue bean="${libroInstance}" field="descripcion" />
                    </h2>

                </g:if>

                <g:form>
                    <g:hiddenField name="id" value="${libroInstance?.id}" />
                    <div class="form-actions">
                        <g:link class="btn" action="edit" id="${libroInstance?.id}">
                            <i class="icon-pencil"></i>
                            <g:message code="default.button.edit.label" default="Edit" />
                        </g:link>
                        <sec:ifAnyGranted roles="ROLE_ADMIN">
                            <button class="btn btn-danger" type="submit" name="_action_delete">
                                <i class="icon-trash icon-white"></i>
                                <g:message code="default.button.delete.label" default="Delete" />
                            </button>
                        </sec:ifAnyGranted>
                    </div>
                </g:form>


                <div class="bs-example bs-example-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                        <g:each status="i" in="${master.epigrafes}" var="e">
                            <li ${i==0?'class="active"':''}>
                                <a href="#${e.keyword}" data-toggle="tab"> ${e.titulo}</a>
                            </li>
                        </g:each>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                    <g:each status="i" in="${master.epigrafes}" var="e">
                        <div class="tab-pane fade in ${i==0?'active':''}" id="${e.keyword}">
                            <g:if test="${e.propuestas}">
                                <markdown:renderHtml text="${e.descripcion}" />
                                <h4 style="color: #777">Propuestas</h4>
                                    <ul>
                                    <g:if test="${libroInstance}">
                                        <g:each
                                            in="${libroInstance.propuestas.findAll{it.epigrafe==e}.sort()}"
                                                var="r">
                                                    <li><markdown:renderHtml text="${r.contenido}" /></li>
                                        </g:each>
                                    </g:if>
                                    <g:else>
                                        <g:each status="j" in="${e.propuestas.sort()}" var="r">
                                            <li><markdown:renderHtml text="${r.contenido}" />
                                            <g:if test="${r.ejemplos}">
                                                <div id="p${i}-${j}" class="ejemplo">
                                                    <markdown:renderHtml text="${r.ejemplos}" />
                                                </div>
                                            </g:if></li>
                                        </g:each>
                                    </g:else>
                                    </ul>
                            </g:if>
                            <g:else>
                                <g:if test="${libroInstance}">
                                    <h2>${libroInstance.titulo}</h2>
                                    <markdown:renderHtml text="${libroInstance.descripcion}" />
                                </g:if>
                                <g:else>
                                    <markdown:renderHtml text="${master.descripcion}" />
                                </g:else>
                            </g:else>
                        </div>
                    </g:each>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
