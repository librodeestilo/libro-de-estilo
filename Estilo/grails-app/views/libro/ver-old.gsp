<%@ page import="com.librodeestilo.Master" %>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'libro.label', default: 'Libro')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="row-fluid">

		<div class="span3">
			<div class="well">
				<ul class="nav nav-list">
					<li class="nav-header">
						${entityName}
					</li>
					<li><g:link class="list" action="list">
							<i class="icon-list"></i>
							<g:message code="default.list.label" args="[entityName]" />
						</g:link></li>
					<li><g:link class="create" action="create">
							<i class="icon-plus"></i>
							<g:message code="default.create.label" args="[entityName]" />
						</g:link></li>
				</ul>
			</div>
		</div>

		<div class="span9">

			<div class="page-header">
				<h1>
					<g:message code="default.show.label" args="[entityName]" />
				</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<dl>

				<g:if test="${libroInstance?.descripcion}">
					<dt>
						<g:message code="libro.descripcion.label" default="Descripción" />
					</dt>

					<dd>
						<g:fieldValue bean="${libroInstance}" field="descripcion" />
					</dd>

				</g:if>

				<g:if test="${libroInstance?.propuestas}">
					<g:each in="${master.epigrafes}" var="e">
						<dt>
							${e.contenido}
						</dt>

						<g:each in="${e.recomendaciones}" var="r">
							<dd>
								<g:if test="${r in libroInstance.propuestas}">
							V->
							</g:if>
								${r.contenido}
							</dd>
						</g:each>
					</g:each>
				</g:if>

			</dl>
		</div>

	</div>
</body>
</html>
