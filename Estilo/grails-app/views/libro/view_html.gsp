<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<%@ page import="com.librodeestilo.Epigrafe"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta name="layout" content="lde-pdf"/>
        <title>Libro de estilo</title>


        <style type="text/css">
            @page {
            size: 210mm 297mm;
            }
            li {
            page-break-inside: avoid;
            }

            .cab-lde
            {
            font-family:	Roboto, Helvetica, Arial, sans-serif;
            font-size:		6rem;
            font-weight:	lighter;
            text-align:		center;
            padding:            3rem;
            border-bottom:      1px solid #000;
            }

        </style>
    </head>
    <body>
        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">
                <div style="page-break-after: always;">
                    <br/>
                    <img class="centered" src="${createLink(controller:'libro', action:'logo')}?slug=${medio.slug}" title="${medio.nombre}"/>
                    
                    <!--<div style="padding:2rem;font-family:Roboto, Helvetica, Arial, sans-serif;font-size:6rem; font-weight:lighter;text-align:center;">-->
                    <div class="cab-lde">
                        Libro de estilo
                    </div>
                    <div class="lde-url">http://www.librodeestilo/${medio.slug?:medio.responsable.username}</div>

                    <pre class="lde-p">${medio.descripcion}</pre>
                    
                    <div class="lde-h1" style="padding-top:10rem">Introducción</div>

                    <div class="lde-p"><markdown:renderHtml text="${libroInstance.descripcion}"/></div>
                </div>

                <div style="page-break-after: always;">
                    <div class="lde-h1">Estilo</div>
                    <hr/>
                    <g:each status="i" in="${['firma','titulares','actualizacion','enlace']}" var="k">
                        <g:set var="e" value="${Epigrafe.findByKeyword(k)}"/>
                        <g:if test="${e.propuestas}">
                            <div class="epigrafe">
                                <div style="page-break-inside: avoid;">
                                    <div class="epi-neg">${e.titulo}</div>
                                    <markdown:renderHtml text="${e.descripcion}" />
                                    <ul>
                                        <g:if test="${libroInstance}">

                                            <g:each
                                            in="${libroInstance.estilo.findAll{it.epigrafe==e}.sort()}"
                                                var="r">
                                                <li><markdown:renderHtml text="${r.contenido}" /></li>
                                                </g:each>
                                            </g:if>
                                            <g:else>
                                                <g:each status="j" in="${e.propuestas.sort()}" var="r">
                                                <li><markdown:renderHtml text="${r.contenido}" /></li>

                                                </g:each>
                                            </g:else>
                                    </ul>
                                </div>
                            </div>
                        </g:if>
                    </g:each>
                </div>

                <div style="page-break-after: always;">              
                    <div class="lde-h1">Ética</div>
                    <hr/>
                    <g:each status="i" in="['fuentes','error','verificacion','plagio', 'redes']" var="k">
                        <g:set var="e" value="${Epigrafe.findByKeyword(k)}"/>
                        <g:if test="${e.propuestas}">
                            <div class="epigrafe">
                                <div style="page-break-inside: avoid;">
                                    <div class="epi-neg">${e.titulo}</div>
                                    <markdown:renderHtml text="${e.descripcion}" />
                                    <ul>
                                        <g:if test="${libroInstance}">

                                            <g:each
                                            in="${libroInstance.etica.findAll{it.epigrafe==e}.sort()}"
                                                var="r">
                                                <li><markdown:renderHtml text="${r.contenido}" /></li>
                                                </g:each>
                                            </g:if>
                                            <g:else>
                                                <g:each status="j" in="${e.propuestas.sort()}" var="r">
                                                <li><markdown:renderHtml text="${r.contenido}" /></li>

                                                </g:each>
                                            </g:else>
                                    </ul>
                                </div>
                            </div>
                        </g:if>
                    </g:each>
                </div>
            </div>
            <div class="span2"></div>
        </div>
    </body>
</html>
