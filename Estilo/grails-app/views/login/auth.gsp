<html>

    <head>
        <title><g:message code='spring.security.ui.login.title'/></title>
        <meta name='layout' content='lde'/>
    </head>

    <body>

        <div class="row-fluid">
            <div class="span2"></div>

            <div class="span8">

                <div class="login s2ui_center ui-corner-all" style='text-align:center;'>
                    <div class="login-inner">
                        <form action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='off'>
                            <div class="sign-in">

                                <div class="headline">
                                <h1>Acceder</h1>
                                </div>
                               
                                <div class="info">
                                <h2>Introduce tus credenciales</h2>
                                </div>
                                
                                <table class="table">
                                    <tr>
                                        <td class="form"><label>Nombre de usuario</label></td>
                                        <td><input style="width:100%" name="j_username" id="username"/></td>
                                    </tr>
                                    <tr>
                                        <td class="form"><label>Contraseña</label></td>
                                        <td><input style="width:100%" type="password" name="j_password" id="password"/></td>
                                    <tr>
                                        <td>&nbsp</td>
                                        <td>
                                    <s2ui:submitButton elementId='loginButton' style='width:100%' class='btn btn-large btn-block' form='loginForm' messageCode='spring.security.ui.login.login'/>
                                    </td>
                                    </tr>
                                    <tr>
                                                                                <td>&nbsp;</td>
                                        <td>
                                            <!--
                                            <input type="checkbox" class="checkbox" name="${rememberMeParameter}" id="remember_me" checked="checked" />
                                            <label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label>
                                            -->
                                            <span class="forgot-link">
                                                <g:link class="btn btn-block" style="color:#000;background-color:#FFF" controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                            <td>
                                        <s2ui:linkButton class="btn btn-block" style="color:#000;background-color:#FFF" elementId='register' controller='register' messageCode='spring.security.ui.login.register'/>
                                        </td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>

            </div>
        </div>



        <script>
            $(document).ready(function() {
            $('#username').focus();
            });

<s2ui:initCheckboxes/>

        </script>

    </body>
</html>
