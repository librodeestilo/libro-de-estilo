<%@ page import="com.librodeestilo.Master" %>



<div class="fieldcontain ${hasErrors(bean: masterInstance, field: 'descripcion', 'error')} ">
	<label for="descripcion">
		<g:message code="master.descripcion.label" default="Descripcion" />
		
	</label>
	<g:textField name="descripcion" value="${masterInstance?.descripcion}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: masterInstance, field: 'epigrafes', 'error')} ">
	<label for="epigrafes">
		<g:message code="master.epigrafes.label" default="Epigrafes" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${masterInstance?.epigrafes?}" var="e">
    <li><g:link controller="epigrafe" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="epigrafe" action="create" params="['master.id': masterInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'epigrafe.label', default: 'Epigrafe')])}</g:link>
</li>
</ul>

</div>

