<%@ page import="com.librodeestilo.Medio" %>



<div class="fieldcontain ${hasErrors(bean: medioInstance, field: 'nombre', 'error')} ">
	<label for="nombre">
		<g:message code="medio.nombre.label" default="Nombre" />
		
	</label>
	<g:textField name="nombre" value="${medioInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medioInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="medio.url.label" default="Url" />
		
	</label>
	<g:textField name="url" value="${medioInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medioInstance, field: 'descripcion', 'error')} ">
	<label for="descripcion">
		<g:message code="medio.descripcion.label" default="Descripcion" />
		
	</label>
	<g:textField name="descripcion" value="${medioInstance?.descripcion}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medioInstance, field: 'responsable', 'error')} ">
	<label for="responsable">
		<g:message code="medio.responsable.label" default="Responsable" />
		
	</label>
	<g:select id="responsable" name="responsable.id" from="${com.librodeestilo.auth.User.list()}" optionKey="id" value="${medioInstance?.responsable?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

