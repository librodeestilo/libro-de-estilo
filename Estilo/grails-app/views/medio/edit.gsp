<%@ page import="com.librodeestilo.Medio"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'medio.label', default: 'Medio')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
<ckeditor:resources />
</head>
<body>
	<div class="row-fluid">

		<div class="span3">
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">
							${entityName}
						</li>
						<li><g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link></li>
						<li><g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link></li>
					</ul>
				</div>
			</sec:ifAnyGranted>
		</div>

		<div class="span9">

			<div class="page-header">
				<h1>Medio</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${medioInstance}">
				<bootstrap:alert class="alert-error">
					<ul>
						<g:eachError bean="${medioInstance}" var="error">
							<li
								<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
									error="${error}" /></li>
						</g:eachError>
					</ul>
				</bootstrap:alert>
			</g:hasErrors>

			<fieldset>
				<g:form class="form-horizontal" action="edit"
					id="${medioInstance?.id}">
					<g:hiddenField name="version" value="${medioInstance?.version}" />
					<fieldset>
						<f:with bean="medioInstance">
							<f:field property="nombre" />
						    <f:field property="slug"/>
							<f:field property="url" />
							<f:field property="descripcion" label="Descripción">
								<g:textArea name="${property}" value="${value}"
									class="input-block-level" style="height:300px;"></g:textArea>
							</f:field>
						</f:with>
						<g:if test="${medioInstance.libro}">
							<g:link controller="libro" action="edit"
								id="${medioInstance.libro?.id}">Editar libro</g:link>
						</g:if>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">
								<i class="icon-ok icon-white"></i>
								<g:message code="default.button.update.label" default="Update" />
							</button>
							<sec:ifAnyGranted roles="ROLE_ADMIN">
								<button type="submit" class="btn btn-danger"
									name="_action_delete" formnovalidate>
									<i class="icon-trash icon-white"></i>
									<g:message code="default.button.delete.label" default="Delete" />
								</button>
							</sec:ifAnyGranted>
						</div>
					</fieldset>
				</g:form>
			</fieldset>

		</div>

	</div>
</body>
</html>
