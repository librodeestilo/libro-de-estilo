<%@ page import="com.librodeestilo.Master"%>
<%@ page import="com.librodeestilo.Libro"%>
<!doctype html>
<html>
    <head>
        <g:set var="entityName"
        value="${message(code: 'libro.label', default: 'Libro')}" />
        <title>Libro de estilo: Ficha</title>
    </head>
    <body>
        <div class="row-fluid">

            <div class="span2">
            </div>

            <div class="span8">
                <g:if test="${flash.message}">
                    <bootstrap:alert class="alert-info">
                        ${flash.message}
                    </bootstrap:alert>
                </g:if>

                <g:hasErrors bean="${libroInstance}">
                    <bootstrap:alert class="alert-error">
                        <ul>
                            <g:eachError bean="${libroInstance}" var="error">
                                <li
                                <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                error="${error}" /></li>
                            </g:eachError>
                        </ul>
                    </bootstrap:alert>
                </g:hasErrors>


                <div class="num">1</div>

                <h1>Ficha</h1>

                <hr/>

                <h2>Edita el nombre del medio, su dirección web, la url en librodeestilo.com y las 
                    personas que han colaborado.
                </h2>

                <g:uploadForm class="form" action="edit_1">
                <!--<g:form class="form" action="edit_1">-->
                    <g:hiddenField name="version" value="${medioInstance?.version}" />
                    <g:hiddenField name="id" value="${medioInstance?.id}" />
                    <table class="table" border="0">
                        <tr>
                            <td class="form">Nombre del medio</td>
                            <td><g:textField name="nombre" value="${medioInstance?.nombre}"/></td>
                        </tr>
                        <tr>
                            <td class="form">URL</td>
                            <td><g:textField name="url" value="${medioInstance?.url}"/></td>
                        </tr>
                        <tr>
                            <td class="form">
                                <g:if test="${medioInstance.logo && medioInstance.slug}">
                                 <img height="50" src="${createLink(controller:'libro', action:'logo')}?slug=${medioInstance.slug}" title="Logo"/>
                                 </g:if>
                                 <g:else>
                                Logo
                                 </g:else>
                                 </td>
                            <td><input type="file" name="logo"/></td>
                        </tr>
                        <tr>
                            <td class="form">URL en librodestilo.com</td>
                            <td>librodeestilo.com/<g:textField name="slug" value="${medioInstance?.slug}"/>.pdf</td>
                        </tr>
                        <tr>
                            <td class="form">Autores</td>
                            <td>
                                <g:textArea name="descripcion" value="${medioInstance?.descripcion}"
                                    class="input-block-level" style="height:300px;">
                                </g:textArea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <g:submitButton class="btn btn-block" name="actualizar" value="Enviar" />
                            </td>
                        </tr>
                    </table>
                <!--</g:form>-->
                </g:uploadForm>
            </div>
            <div class="span2">
            </div>
        </div>
    </body>
</html>
