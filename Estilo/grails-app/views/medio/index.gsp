
<%@ page import="com.librodeestilo.Medio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'medio.label', default: 'Medio')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-medio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-medio" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="nombre" title="${message(code: 'medio.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="url" title="${message(code: 'medio.url.label', default: 'Url')}" />
					
						<g:sortableColumn property="descripcion" title="${message(code: 'medio.descripcion.label', default: 'Descripcion')}" />
					
						<th><g:message code="medio.responsable.label" default="Responsable" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${medioInstanceList}" status="i" var="medioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${medioInstance.id}">${fieldValue(bean: medioInstance, field: "nombre")}</g:link></td>
					
						<td>${fieldValue(bean: medioInstance, field: "url")}</td>
					
						<td>${fieldValue(bean: medioInstance, field: "descripcion")}</td>
					
						<td>${fieldValue(bean: medioInstance, field: "responsable")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${medioInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
