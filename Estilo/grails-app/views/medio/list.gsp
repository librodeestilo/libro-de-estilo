
<%@ page import="com.librodeestilo.Medio" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'medio.label', default: 'Medio')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="nombre" title="${message(code: 'medio.nombre.label', default: 'Nombre')}" />
						
							<g:sortableColumn property="url" title="${message(code: 'medio.url.label', default: 'Url')}" />
						
							<g:sortableColumn property="descripcion" title="${message(code: 'medio.descripcion.label', default: 'Descripcion')}" />
						
							<th class="header"><g:message code="medio.responsable.label" default="Responsable" /></th>
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${medioInstanceList}" var="medioInstance">
						<tr>
						
							<td>${fieldValue(bean: medioInstance, field: "nombre")}</td>
						
							<td>${fieldValue(bean: medioInstance, field: "url")}</td>
						
							<td>${fieldValue(bean: medioInstance, field: "descripcion")}</td>
						
							<td>${fieldValue(bean: medioInstance, field: "responsable")}</td>
						
							<td class="link">
								<g:link action="show" id="${medioInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${medioInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
