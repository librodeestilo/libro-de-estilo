
<%@ page import="com.librodeestilo.Medio" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'medio.label', default: 'Medio')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
				</sec:ifAnyGranted>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1>${medioInstance?.nombre}</h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>				
				
									<g:if test="${medioInstance?.slug}">
						<dt><g:message code="medio.slug.label" default="Slug" /></dt>
						
							<dd><g:fieldValue bean="${medioInstance}" field="slug"/></dd>
						
					</g:if>
				
					<g:if test="${medioInstance?.url}">
						<dt><g:message code="medio.url.label" default="URL" /></dt>
						
							<dd><g:fieldValue bean="${medioInstance}" field="url"/></dd>
						
					</g:if>
				
					<g:if test="${medioInstance?.descripcion}">
						<dt><g:message code="medio.descripcion.label" default="Descripción" /></dt>
						
							<dd><g:fieldValue bean="${medioInstance}" field="descripcion"/></dd>
						
					</g:if>
				
					<g:if test="${medioInstance?.responsable}">
						<dt><g:message code="medio.responsable.label" default="Responsable" /></dt>
						
							<dd>${medioInstance?.responsable?.encodeAsHTML()}</dd>
						
					</g:if>

					<g:if test="${medioInstance?.libro}">
						<dt><g:message code="medio.libro.label" default="Libro" />
										<g:link class="btn" controller="libro" action="edit" id="${medioInstance?.libro?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</dt>
						
							<dd>
											<ul>
				<g:set var="link" value="${grailsApplication.config.grails.serverURL}"/>
				<g:set var="slug" value="${medioInstance.slug}"/>
				<li><a href="${link}/libro/${slug?:'slug'}.pdf">${link}/libro/${slug?:'slug'}.pdf</a></li>
				<li><a href="${link}/libro/${slug?:'slug'}.html">${link}/libro/${slug?:'slug'}.html</a></li>				
				</ul>

							</dd>
						
					</g:if>

				</dl>

				<g:form>
					<g:hiddenField name="id" value="${medioInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn btn-primary" action="edit" id="${medioInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<sec:ifAnyGranted roles="ROLE_ADMIN">
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
						</sec:ifAnyGranted>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
