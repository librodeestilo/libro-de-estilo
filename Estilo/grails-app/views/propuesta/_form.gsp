<%@ page import="com.librodeestilo.Propuesta" %>



<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="propuesta.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${propuestaInstance.orden}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'nivel', 'error')} required">
	<label for="nivel">
		<g:message code="propuesta.nivel.label" default="Nivel" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="nivel" type="number" value="${propuestaInstance.nivel}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'fecha', 'error')} required">
	<label for="fecha">
		<g:message code="propuesta.fecha.label" default="Fecha" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fecha" precision="day"  value="${propuestaInstance?.fecha}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'actual', 'error')} ">
	<label for="actual">
		<g:message code="propuesta.actual.label" default="Actual" />
		
	</label>
	<g:checkBox name="actual" value="${propuestaInstance?.actual}" />
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'contenido', 'error')} ">
	<label for="contenido">
		<g:message code="propuesta.contenido.label" default="Contenido" />
		
	</label>
	<g:textField name="contenido" value="${propuestaInstance?.contenido}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'ejemplos', 'error')} ">
	<label for="ejemplos">
		<g:message code="propuesta.ejemplos.label" default="Ejemplos" />
		
	</label>
	<g:textField name="ejemplos" value="${propuestaInstance?.ejemplos}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'epigrafe', 'error')} required">
	<label for="epigrafe">
		<g:message code="propuesta.epigrafe.label" default="Epigrafe" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="epigrafe" name="epigrafe.id" from="${com.librodeestilo.Epigrafe.list()}" optionKey="id" required="" value="${propuestaInstance?.epigrafe?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: propuestaInstance, field: 'preguntas', 'error')} ">
	<label for="preguntas">
		<g:message code="propuesta.preguntas.label" default="Preguntas" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${propuestaInstance?.preguntas?}" var="p">
    <li><g:link controller="QA" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="QA" action="create" params="['propuesta.id': propuestaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'QA.label', default: 'QA')])}</g:link>
</li>
</ul>

</div>

