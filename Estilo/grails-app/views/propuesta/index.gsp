
<%@ page import="com.librodeestilo.Propuesta" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'propuesta.label', default: 'Propuesta')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-propuesta" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-propuesta" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="orden" title="${message(code: 'propuesta.orden.label', default: 'Orden')}" />
					
						<g:sortableColumn property="nivel" title="${message(code: 'propuesta.nivel.label', default: 'Nivel')}" />
					
						<g:sortableColumn property="fecha" title="${message(code: 'propuesta.fecha.label', default: 'Fecha')}" />
					
						<g:sortableColumn property="actual" title="${message(code: 'propuesta.actual.label', default: 'Actual')}" />
					
						<g:sortableColumn property="contenido" title="${message(code: 'propuesta.contenido.label', default: 'Contenido')}" />
					
						<g:sortableColumn property="ejemplos" title="${message(code: 'propuesta.ejemplos.label', default: 'Ejemplos')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${propuestaInstanceList}" status="i" var="propuestaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${propuestaInstance.id}">${fieldValue(bean: propuestaInstance, field: "orden")}</g:link></td>
					
						<td>${fieldValue(bean: propuestaInstance, field: "nivel")}</td>
					
						<td><g:formatDate date="${propuestaInstance.fecha}" /></td>
					
						<td><g:formatBoolean boolean="${propuestaInstance.actual}" /></td>
					
						<td>${fieldValue(bean: propuestaInstance, field: "contenido")}</td>
					
						<td>${fieldValue(bean: propuestaInstance, field: "ejemplos")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${propuestaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
