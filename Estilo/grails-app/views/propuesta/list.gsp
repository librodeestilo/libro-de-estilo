
<%@ page import="com.librodeestilo.Propuesta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'propuesta.label', default: 'Propuesta')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="orden" title="${message(code: 'propuesta.orden.label', default: 'Orden')}" />
						
							<g:sortableColumn property="nivel" title="${message(code: 'propuesta.nivel.label', default: 'Nivel')}" />
						
							<g:sortableColumn property="fecha" title="${message(code: 'propuesta.fecha.label', default: 'Fecha')}" />
						
							<g:sortableColumn property="actual" title="${message(code: 'propuesta.actual.label', default: 'Actual')}" />
						
							<g:sortableColumn property="contenido" title="${message(code: 'propuesta.contenido.label', default: 'Contenido')}" />
						
							<g:sortableColumn property="ejemplos" title="${message(code: 'propuesta.ejemplos.label', default: 'Ejemplos')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${propuestaInstanceList}" var="propuestaInstance">
						<tr>
						
							<td>${fieldValue(bean: propuestaInstance, field: "orden")}</td>
						
							<td>${fieldValue(bean: propuestaInstance, field: "nivel")}</td>
						
							<td><g:formatDate date="${propuestaInstance.fecha}" /></td>
						
							<td><g:formatBoolean boolean="${propuestaInstance.actual}" /></td>
						
							<td>${fieldValue(bean: propuestaInstance, field: "contenido")}</td>
						
							<td>${fieldValue(bean: propuestaInstance, field: "ejemplos")}</td>
						
							<td class="link">
								<g:link action="show" id="${propuestaInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${propuestaInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
