
<%@ page import="com.librodeestilo.Propuesta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'propuesta.label', default: 'Propuesta')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${propuestaInstance?.orden}">
						<dt><g:message code="propuesta.orden.label" default="Orden" /></dt>
						
							<dd><g:fieldValue bean="${propuestaInstance}" field="orden"/></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.nivel}">
						<dt><g:message code="propuesta.nivel.label" default="Nivel" /></dt>
						
							<dd><g:fieldValue bean="${propuestaInstance}" field="nivel"/></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.fecha}">
						<dt><g:message code="propuesta.fecha.label" default="Fecha" /></dt>
						
							<dd><g:formatDate date="${propuestaInstance?.fecha}" /></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.actual}">
						<dt><g:message code="propuesta.actual.label" default="Actual" /></dt>
						
							<dd><g:formatBoolean boolean="${propuestaInstance?.actual}" /></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.contenido}">
						<dt><g:message code="propuesta.contenido.label" default="Contenido" /></dt>
						
							<dd><g:fieldValue bean="${propuestaInstance}" field="contenido"/></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.ejemplos}">
						<dt><g:message code="propuesta.ejemplos.label" default="Ejemplos" /></dt>
						
							<dd><g:fieldValue bean="${propuestaInstance}" field="ejemplos"/></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.epigrafe}">
						<dt><g:message code="propuesta.epigrafe.label" default="Epigrafe" /></dt>
						
							<dd><g:link controller="epigrafe" action="show" id="${propuestaInstance?.epigrafe?.id}">${propuestaInstance?.epigrafe?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${propuestaInstance?.preguntas}">
						<dt><g:message code="propuesta.preguntas.label" default="Preguntas" /></dt>
						
							<g:each in="${propuestaInstance.preguntas}" var="p">
							<dd><g:link controller="QA" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${propuestaInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${propuestaInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
