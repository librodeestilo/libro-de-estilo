<html>

    <head>
        <title><g:message code='spring.security.ui.forgotPassword.title'/></title>
        <meta name='layout' content='lde'/>
    </head>

    <body>

        <p/>

        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">

                <div class="headline">
                    <h1>Recuperación de contraseña</h1>
                </div>

                <div class="info">
                    <h2>Introduce tu nombre de usuario. Recibirás un mensaje con un enlace para cambiar la contraseña.</h2>
                </div>


                <div style="margin:auto">
                    <s2ui:form width='400' height='220' elementId='forgotPasswordFormContainer'
                    titleCode='' center='true'>

                    <g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off'>

                        <g:if test='${emailSent}'>
                            <br/>
                            <g:message code='spring.security.ui.forgotPassword.sent'/>
                        </g:if>

                        <g:else>

                            <br/>
                            <!--
                            <h4><g:message code='spring.security.ui.forgotPassword.description'/></h4>
                            -->
                            <table class="table" style="margin:auto">
                                <tr>
                                    <td><label for="username"><g:message code='spring.security.ui.forgotPassword.username'/></label></td>
                                    <td><g:textField name="username" style="width:100%" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                <s2ui:submitButton class="btn btn-block"elementId='reset' form='forgotPasswordForm' messageCode='spring.security.ui.forgotPassword.submit'/>
                                </td>
                                </tr>
                            </table>


                        </g:else>

                    </g:form>
                    </s2ui:form>
                </div>

            </div>
            <div class="span2"></div>
        </div>
        <script>
            $(document).ready(function() {
            $('#username').focus();
            });
        </script>

    </body>
</html>
