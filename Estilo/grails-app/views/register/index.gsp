<html>

    <head>
        <meta name='layout' content='lde'/>
        <title><g:message code='spring.security.ui.register.title'/></title>
    </head>

    <body>
        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">

                <div class="headline">
                    <h1>Crear cuenta</h1>
                </div>

                <div class="info">
                    <h2>Introduce nombre de usuario, email y contraseña. Recibirás un mensaje de confirmación.</h2>
                </div>

                <p/>

                <s2ui:form width='650' height='300' elementId='loginFormContainer'
                titleCode='' center='true'>

                <g:form action='register' name='registerForm'>

                    <g:if test='${emailSent}'>
                        <br/>
                        <g:message code='spring.security.ui.register.sent'/>
                    </g:if>
                    <g:else>

                        <br/>

                        <table class="table" style="margin:auto" style="width:80%">
                            <tbody>

                            <s2ui:textFieldRow name='username' labelCode='user.username.label' bean="${command}"
                                               style="width:100%" labelCodeDefault='Username' value="${command.username}"/>

                            <s2ui:textFieldRow name='email' bean="${command}" value="${command.email}"
                                               style="width:100%" labelCode='user.email.label' labelCodeDefault='E-mail'/>

                                               <s2ui:passwordFieldRow name='password' labelCode='user.password.label' bean="${command}"
                                               style="width:100%" labelCodeDefault='Password' value="${command.password}"/>

                            <s2ui:passwordFieldRow name='password2' labelCode='user.password2.label' bean="${command}"
                                                   style="width:100%" labelCodeDefault='Password (again)' value="${command.password2}"/>

                                                   <tr>
                                                       <td>&nbsp;</td>
                                                       <td>
                               <s2ui:submitButton elementId='create' class="btn btn-block" form='registerForm' messageCode='spring.security.ui.register.submit'/>
                               </td>
                               </tr>
                               
                            </tbody>
                        </table>


                    </g:else>

                </g:form>

                </s2ui:form>
            </div>
            <div class="span2"></div>
        </div>


        <script>
            $(document).ready(function() {
            $('#username').focus();
            });
        </script>


    </body>
</html>
