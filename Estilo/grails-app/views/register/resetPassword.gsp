<html>

    <head>
        <title><g:message code='spring.security.ui.resetPassword.title'/></title>
        <meta name='layout' content='lde'/>
    </head>

    <body>

        <p/>

        <div class="row-fluid">
            <div class="span2"></div>
            <div class="span8">

                <div class="headline">
                    <h1>Nueva contraseña</h1>
                </div>

                <div class="info">
                    <h2>Introduce nueva contraseña</h2>
                </div>


                <s2ui:form width='475' height='250' elementId='resetPasswordFormContainer'
                titleCode='' center='true'>

                <g:form action='resetPassword' name='resetPasswordForm' autocomplete='off'>
                    <g:hiddenField name='t' value='${token}'/>
                    <div class="sign-in">

                        <br/>
                        <h4><g:message code='spring.security.ui.resetPassword.description'/></h4>

                        <table class="table">
                            <s2ui:passwordFieldRow name='password' labelCode='resetPasswordCommand.password.label' bean="${command}"
                                                   labelCodeDefault='Password' value="${command?.password}"/>

                            <s2ui:passwordFieldRow name='password2' labelCode='resetPasswordCommand.password2.label' bean="${command}"
                                                   labelCodeDefault='Password (again)' value="${command?.password2}"/>

                            <tr>
                                <td>&nbsp;</td>
                                <td>
                            <s2ui:submitButton class="btn btn-block" elementId='reset' form='resetPasswordForm' messageCode='spring.security.ui.resetPassword.submit'/>
                            </td>
                            </tr>
                        </table>


                    </div>
                </g:form>

                </s2ui:form>

            </div>
            <div class="span2"></div>
        </div>


        <script>
            $(document).ready(function() {
            $('#password').focus();
            });
        </script>

    </body>
</html>
