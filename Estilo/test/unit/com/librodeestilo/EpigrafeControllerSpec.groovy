package com.librodeestilo



import grails.test.mixin.*
import spock.lang.*

@TestFor(EpigrafeController)
@Mock(Epigrafe)
class EpigrafeControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.epigrafeInstanceList
            model.epigrafeInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.epigrafeInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def epigrafe = new Epigrafe()
            epigrafe.validate()
            controller.save(epigrafe)

        then:"The create view is rendered again with the correct model"
            model.epigrafeInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            epigrafe = new Epigrafe(params)

            controller.save(epigrafe)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/epigrafe/show/1'
            controller.flash.message != null
            Epigrafe.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def epigrafe = new Epigrafe(params)
            controller.show(epigrafe)

        then:"A model is populated containing the domain instance"
            model.epigrafeInstance == epigrafe
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def epigrafe = new Epigrafe(params)
            controller.edit(epigrafe)

        then:"A model is populated containing the domain instance"
            model.epigrafeInstance == epigrafe
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/epigrafe/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def epigrafe = new Epigrafe()
            epigrafe.validate()
            controller.update(epigrafe)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.epigrafeInstance == epigrafe

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            epigrafe = new Epigrafe(params).save(flush: true)
            controller.update(epigrafe)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/epigrafe/show/$epigrafe.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/epigrafe/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def epigrafe = new Epigrafe(params).save(flush: true)

        then:"It exists"
            Epigrafe.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(epigrafe)

        then:"The instance is deleted"
            Epigrafe.count() == 0
            response.redirectedUrl == '/epigrafe/index'
            flash.message != null
    }
}
