package com.librodeestilo



import grails.test.mixin.*
import spock.lang.*

@TestFor(MasterController)
@Mock(Master)
class MasterControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.masterInstanceList
            model.masterInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.masterInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def master = new Master()
            master.validate()
            controller.save(master)

        then:"The create view is rendered again with the correct model"
            model.masterInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            master = new Master(params)

            controller.save(master)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/master/show/1'
            controller.flash.message != null
            Master.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def master = new Master(params)
            controller.show(master)

        then:"A model is populated containing the domain instance"
            model.masterInstance == master
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def master = new Master(params)
            controller.edit(master)

        then:"A model is populated containing the domain instance"
            model.masterInstance == master
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/master/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def master = new Master()
            master.validate()
            controller.update(master)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.masterInstance == master

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            master = new Master(params).save(flush: true)
            controller.update(master)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/master/show/$master.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/master/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def master = new Master(params).save(flush: true)

        then:"It exists"
            Master.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(master)

        then:"The instance is deleted"
            Master.count() == 0
            response.redirectedUrl == '/master/index'
            flash.message != null
    }
}
