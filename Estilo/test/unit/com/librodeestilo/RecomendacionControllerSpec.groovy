package com.librodeestilo



import grails.test.mixin.*
import spock.lang.*

@TestFor(PropuestaController)
@Mock(Propuesta)
class RecomendacionControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.recomendacionInstanceList
            model.recomendacionInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.recomendacionInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def recomendacion = new Propuesta()
            recomendacion.validate()
            controller.save(recomendacion)

        then:"The create view is rendered again with the correct model"
            model.recomendacionInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            recomendacion = new Propuesta(params)

            controller.save(recomendacion)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/recomendacion/show/1'
            controller.flash.message != null
            Propuesta.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def recomendacion = new Propuesta(params)
            controller.show(recomendacion)

        then:"A model is populated containing the domain instance"
            model.recomendacionInstance == recomendacion
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def recomendacion = new Propuesta(params)
            controller.edit(recomendacion)

        then:"A model is populated containing the domain instance"
            model.recomendacionInstance == recomendacion
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/recomendacion/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def recomendacion = new Propuesta()
            recomendacion.validate()
            controller.update(recomendacion)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.recomendacionInstance == recomendacion

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            recomendacion = new Propuesta(params).save(flush: true)
            controller.update(recomendacion)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/recomendacion/show/$recomendacion.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/recomendacion/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def recomendacion = new Propuesta(params).save(flush: true)

        then:"It exists"
            Propuesta.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(recomendacion)

        then:"The instance is deleted"
            Propuesta.count() == 0
            response.redirectedUrl == '/recomendacion/index'
            flash.message != null
    }
}
